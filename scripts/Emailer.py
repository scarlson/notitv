#!/usr/bin/env python
try:
    import datetime
    today = datetime.date.today()

    print "Starting", today.strftime("%a %b %d")

    import smtplib
    import psycopg2
    import random
    from models.Show import Show
    from models.User import User
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from tornado import template

    conn = psycopg2.connect('dbname=noti user=postgres')
    conn.set_client_encoding('UTF8')
    cur = conn.cursor()

    smtp_user = 'AKIAJEKGIXC6CQQLNEHQ'
    smtp_pass = 'AkLN6/QTC/BgmIfRCtl0rK6UUni/ZA8dmt3790iqwT8n'
    port = 465
    host = 'email-smtp.us-east-1.amazonaws.com'

    DEBUG = 0
    loader = template.Loader('/var/web/tvdb/email/')

    def unique_hash():
        _hashpool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        _hash = "".join(random.choice(_hashpool) for i in range(8))
        existing = cur.mogrify('select distinct hash from unsub;')
        cur.execute(existing)
        existing = [ha for hashes in cur.fetchall() for ha in hashes] or ""
        if _hash in existing:
            return unique_hash()
        return _hash

    def Unsub(user):
        q = 'select hash from unsub where user_id = %s;'
        q = cur.mogrify(q, (user.user_id, ))
        cur.execute(q)
        q = cur.fetchall()
        if q:
            _hash = q[0][0]
        else:
            q = 'insert into unsub values (%s, %s);'
            _hash = unique_hash()
            q = cur.mogrify(q, (user.user_id, _hash))
            cur.execute(q)
            conn.commit()
        return _hash

    def Emailer(user, shows):
        if shows:
            sender = u'no-reply@noti.tv'
            if DEBUG:
                receivers = ('stevengcarlson@gmail.com')
            else:
                receivers = (user.email)
            data = {}
            data['date'] = today.strftime("%a %b %d")
            data['user'] = user
            data['shows'] = shows
            data['unsub'] = Unsub(user)
            msg = MIMEMultipart('alternative', _charset='UTF-8')
            if DEBUG:
                msg['Subject'] = 'DEBUG ON! Noti.tv ' + today.strftime("%a %b %d") + '.'
            else:
                msg['Subject'] = 'Your Noti.tv notifications for ' + today.strftime("%a %b %d.")
            msg['From'] = u'"Noti.tv" <no-reply@noti.tv>'
            if DEBUG:
                msg['To'] = 'stevengcarlson@gmail.com'
            else:
                msg['To'] = user.email

            body = loader.load("email.html").generate(**data)
            body = MIMEText(body, 'html')
            msg.attach(body)
            s = smtplib.SMTP_SSL(host, port, 'noti.tv')
            s.login(smtp_user, smtp_pass)
            s.sendmail(sender, receivers, msg.as_string())

    def ShowFinder():
        query = cur.mogrify('select distinct show_id from episode where air_date::date = %s;',
                            (today,))
        cur.execute(query)
        results = cur.fetchall()
        todays_shows = []

        for showid in results:
            show = Show(showid[0], db=conn)
            todays_shows.append(show)

        if DEBUG:
            query = "select user_id from users where email = 'stevengcarlson@gmail.com';"
        else:
            query = "select user_id from users where email is not NULL and notifications = TRUE;"
        cur.execute(query)
        results = cur.fetchall()
        users = []
        for uid in results:
            user = User(uid[0], db=conn)
            users.append(user)

        for user in users:
            if user.subs:
                user_todays = [show for show in todays_shows if show in user.subs]
                if user_todays:
                    Emailer(user, user_todays)

        print "Ending", today.strftime("%a %b %d")

    if __name__ == "__main__":
        ShowFinder()
except Exception, e:
    import datetime
    print datetime.date.today(), e
