#!/usr/bin/env python
import urllib
import json
import freebase
import psycopg2
import datetime
from models.Show import Show
from models.Episode import Episode
from models.Idmap import Idmap 

today = datetime.date.today()

conn = psycopg2.connect('dbname=noti user=postgres')
conn.set_client_encoding('UTF8')
cur = conn.cursor()

def qry(query):
    try:
        cur.execute(query)
        conn.commit()
        return True
    except:
        conn.rollback()
        return False

def update_id_map(show):
    if show['show_id']:
        s = Idmap(show['show_id'],key="show_id",table="id_map")
        s.wikipedia = s.wikipedia or show['wikipedia']
        s.thetvdb_id = s.thetvdb_id or show['thetvdb_id']
        s.imdb_id = s.imdb_id or show['imdb_id']
        s.fb_guid = s.fb_guid or show['fb_guid']
        s.hulu = s.hulu or show['hulu']
        s.netflix = s.netflix or show['netflix']
        s.save()

def update_show(show):
    if show['show_id']:
        s = Show(show['show_id'])
        s.name = show['name'] or s.name
        s.description = show['description'] or s.description
        s.network = show['network'] or s.network
        print s.save()
    else:
        return False

def new_show(show):
    s = Show('default')
    s.name = show['name'] or s.name
    s.description = show['description'] or s.description
    s.network = show['network'] or s.network
    id_maps(s.name)

def update_episode(episode):
    q = cur.mogrify('select episode_id from episode where episode_no = %s and season_no = %s and show_id = %s;',        (episode['episode_number'], episode['season_number'], episode['show_id']))
    cur.execute(q)
    try:
        q = cur.fetchone()[0]
    except:
        q = None
    if q:
        q = Episode(q)
        q.name = episode['name'] or q.name
        q.description = episode['description'] or q.description
        q.air_date = episode['air_date'] or q.air_date
        return q.save()
    else:
        query = cur.mogrify("insert into episode (show_id, name, description, air_date, episode_no, season_no) values (%s,%s,%s,%s,%s,%s);", 
        (episode['show_id'], episode['name'], episode['description'], episode['air_date'], episode['episode_number'], episode['season_number']))
        if qry(query):
            print "inserts!", episode['show_id']


today = datetime.datetime.strftime(today,'%Y-%m-%d')


def id_maps(name=None):
  if name:
      query = "select name, show_id from show where name like %s;"
      cur.execute(query,(name,))
  else:
      query = "select name, show_id from show;"
      cur.execute(query)
  names = cur.fetchall()
  if names:
    for name, show_id in names:
        API_KEY = 'AIzaSyAzbqQwegJFPH46MRwGpc4QyzHUO-ygt6E'
        service_url = 'https://www.googleapis.com/freebase/v1/search'
        params = {
          'query': name,
          'filter': '(all type:/tv/tv_program)',
          'limit': 1,
          'key': API_KEY
        }
        url = service_url + '?' + urllib.urlencode(params)
        response = json.loads(urllib.urlopen(url).read())
        if not response:
            return None
        if response['result']:
            response = response['result'][0]
        if 'score' in response and response['score'] > 100:
            name = response['name']
            show_query = {
                'name~=': name,
                'type': '/tv/tv_program',
                'hulu_id': {
                    'value':None,
                    'limit':1
                    },
                'thetvdb_id': None,
                "key": [{
                        "namespace|=" :['/authority/imdb/title',"/wikipedia/en_id","/authority/netflix/tiny"],
                        "namespace": None,
                        "value": None,
                    }],
                'guid': None,
                'air_date_of_final_episode': {"value":None,"optional":"forbidden"},
            }
            try:
                results = freebase.mqlreaditer(show_query)
            except:
                results = None

            if results:
                mappin(results, show_id)
    

def tvdb_map():
    query = "select thetvdb_id, show_id from id_map where thetvdb_id is not null;"
    cur.execute(query)
    names = cur.fetchall()
    for tvdb, show_id in names:
        show_query = {
            'name': None,
            'type': '/tv/tv_program',
            'hulu_id': {
                'value':None,
                'limit':1
                },
            'thetvdb_id': str(tvdb),
            "key": [{
                    "namespace|=" :['/authority/imdb/title',"/wikipedia/en_id","/authority/netflix/tiny"],
                    "namespace": None,
                    "value": None,
                }],
            'guid': None,
            'air_date_of_final_episode': {"value":None,"optional":"forbidden"},
        }
        try:
            results = freebase.mqlreaditer(show_query)
        except:
            results = None

        if results:
            mappin(results, show_id)
    

def mappin(results, show_id):
    for res in results:
        s = {}
        s['show_id'] = show_id
        try:
            s['hulu'] = res['hulu_id']['value']
        except:
            s['hulu'] = res['hulu_id']
        s['fb_guid'] = res['guid']
        s['thetvdb_id'] = res['thetvdb_id']
        s['imdb_id'] = None
        s['wikipedia'] = None
        s['netflix'] = None
        for key in res['key']:
            if key['namespace'] == "/authority/imdb/title":
                s['imdb_id'] = key['value']
            if key['namespace'] == "/wikipedia/en_id":
                s['wikipedia'] = key['value']
            if key['namespace'] == "/authority/netflix/tiny":
                s['netflix'] = key['value']
        try:
            update_id_map(s)
        except psycopg2.ProgrammingError:
            print res
            raise

def shows():
    query = "select fb_guid, show_id from id_map where fb_guid is not null;"
    cur.execute(query)
    names = cur.fetchall()
    for guid, show_id in names:
        show_query = {
            'guid': guid,
            'name': None,
            'type': '/tv/tv_program',
            'air_date_of_final_episode': {"value":None,"optional":"forbidden"},
            'original_network':[{'network':[{'name':None}]}],
            '/common/topic/article': [{
                "id":None,
                "limit":1,
            }],
        }

        try:
            results = freebase.mqlreaditer(show_query)
        except:
            results = None

        if results:
            print 'reslen', len(list(results)), show_id
            for res in results:
                #print res
                show = {}
                show['name'] = res['name']
                try:
                    show['description'] = freebase.raw(res['/common/topic/article'][0]['id']).split('\n')[0][3:]
                except:
                    raise
                    show['description'] = None
                show['network'] = res['original_network'][0]['network'][0]['name']
                show['show_id'] = show_id
                update_show(show)

def all_the_things():
    charlie_foxtrot = [{
        'type':'/tv/tv_series_episode',
        'series':[{
            'name':None,
            'air_date_of_final_episode': {"value":None,"optional":"forbidden"},
            'original_network':[{
                'network':[{
                    'name':None,
                }]
            }],
            '/common/topic/article': [{
                "id":None,
                "limit":1,
            }],    
        }],
        'air_date>=':today,
        'air_date':None,
        'episode_number':None,
        'season_number':None,
        'name':None,
        '/common/topic/description':[],
    }]

    try:
        results = freebase.mqlreaditer(charlie_foxtrot)
    except:
        results = None

    if results:
        for res in results:
            name = res['series'][0]['name']
            show_id = "select show.show_id from show where show.name like %s;"
            show_id = cur.mogrify(show_id, (name,))
            cur.execute(show_id)
            show_id = cur.fetchone()
            if show_id:
                episode = {}
                episode['air_date'] = datetime.datetime.strptime(res['air_date'],'%Y-%m-%d')
                episode['episode_number'] = str(res['episode_number'])
                episode['season_number'] = str(res['season_number'])
                episode['name'] = res['name']
                episode['description'] = None
                episode['show_id'] = show_id[0]
                if res['/common/topic/description']:
                    episode['description'] = res['/common/topic/description'][0]
                
                update_episode(episode)

                show = {}
                show['name'] = res['series'][0]['name']
                show['description'] = freebase.raw(res['series'][0]['/common/topic/article'][0]['id']).split('\n')[0][3:]
                show['network'] = res['series'][0]['original_network'][0]['network'][0]['name']
                show['show_id'] = show_id
                update_show(show)
            else:
                pass
                show = {}
                show['name'] = res['series'][0]['name']
                show['description'] = freebase.raw(res['series'][0]['/common/topic/article'][0]['id']).split('\n')[0][3:]
                show['network'] = res['series'][0]['original_network'][0]['network'][0]['name']
                show = new_show(show)                

                episode = {}
                episode['air_date'] = datetime.datetime.strptime(res['air_date'],'%Y-%m-%d')
                episode['episode_number'] = str(res['episode_number'])
                episode['season_number'] = str(res['season_number'])
                episode['name'] = res['name']
                episode['description'] = None
                episode['show_id'] = show_id[0]
                if res['/common/topic/description']:
                    episode['description'] = res['/common/topic/description'][0]
                
                update_episode(episode)


def episodes():
    query = "select fb_guid, show_id from id_map where fb_guid is not null;"
    cur.execute(query)
    names = cur.fetchall()
    for name in names: 
        charlie_foxtrot = [{
            'type':'/tv/tv_series_episode',
            'series':[{
                'guid': name[0],
                'name': None,
                'air_date_of_final_episode': {"value":None,"optional":"forbidden"},
                'original_network':[{
                    'network':[{
                        'name':None,
                    }]
                }],
                '/common/topic/article': [{
                    "id":None,
                    "limit":1,
                }],    
            }],
            'air_date>=':today,
            'air_date':None,
            'episode_number':None,
            'season_number':None,
            'name':None,
            '/common/topic/description':[],
        }]


        try:
            results = freebase.mqlreaditer(charlie_foxtrot)
        except:
            results = None

        if results:
          try:
            for res in results:
                show_id = name[1]
                if show_id:
                    episode = {}
                    episode['air_date'] = datetime.datetime.strptime(res['air_date'],'%Y-%m-%d')
                    episode['episode_number'] = str(res['episode_number'])
                    episode['season_number'] = str(res['season_number'])
                    episode['name'] = res['name']
                    episode['description'] = None
                    episode['show_id'] = show_id
                    if res['/common/topic/description']:
                        episode['description'] = res['/common/topic/description'][0]
                    
                    update_episode(episode)

                    show = {}
                    show['name'] = res['series'][0]['name']
                    show['description'] = freebase.raw(res['series'][0]['/common/topic/article'][0]['id']).split('\n')[0][3:]
                    show['network'] = res['series'][0]['original_network'][0]['network'][0]['name']
                    show['show_id'] = show_id
                    update_show(show)

          except freebase.api.session.MetawebError, e:
            print e

#shows()
id_maps()
episodes()


'''
    for prog in net['programs']:
        for show in prog['program']:
            for article in show['/common/topic/article']:
                print desc
'''

show = '''

 show_id     | integer                | not null default nextval('show_show_id_seq'::regclass)
 name        | text                   | 
 description | text                   | 
 network     | text                   | 
 dow         | character varying(10)  | 
 time        | time without time zone | 

'''

episode = """

 show_id     | integer                     | 
 episode_id  | integer                     | not null default nextval('episode_episode_id_seq'::regclass)
 name        | text                        | 
 description | text                        | 
 air_date    | timestamp without time zone | 
 episode_no  | character varying(10)       | 
 season_no   | character varying(10)       | 

"""

id_map = """

                            Table "public.id_map"
   Column   |  Type   |                        Modifiers                        
------------+---------+---------------------------------------------------------
 map_id     | integer | not null default nextval('id_map_map_id_seq'::regclass)
 show_id    | integer | 
 wikipedia  | text    | 
 thetvdb_id | integer | 
 imdb_id    | text    | 
 zap2it_id  | text    | 
 fb_guid    | text    | 
 hulu       | text    | 
 netflix    | text    | 

"""
