#!/usr/bin/env python
import Image
import psycopg2
import urllib2
import urllib
import datetime
import os
import sys
from xml.etree import ElementTree
from models.gstore import Bucket

print 'yeap'

bukkit = Bucket()
the_tv_db_api_key = '17F2768FC31BE4AE'
conn = psycopg2.connect('dbname=noti user=postgres')
conn.set_client_encoding('UTF8')
cur = conn.cursor()
today = datetime.date.today()

test = False #True

if not test:
    image_dir = "/var/web/tvdbimages/"
else:
    image_dir = '/home/steve/testinshit/'

sizes = ((170,250),(272,399))

def read(url):
    try:
        return urllib2.urlopen(url).read()
    except urllib2.URLError:
        return None
    except urllib2.HTTPError:
        return None

def get_fanart(xml):
    banner_root = "http://www.thetvdb.com/banners/"
    for series in xml:
        if series.find('fanart').text:
            return banner_root + series.find('fanart').text

def get_poster(xml):
    banner_root = "http://www.thetvdb.com/banners/"
    for series in xml:
        if series.find('poster').text:
            return banner_root + series.find('poster').text

def xml(tvdb_id):
    url = 'http://www.thetvdb.com/api/' + the_tv_db_api_key + '/series/' + str(tvdb_id) + '/en.xml'
    ep = read(url)
    if ep:
        try:
            xml = ElementTree.fromstring(ep)
            xml = ElementTree.ElementTree(xml).getroot()
        except:
            xml = None
        if xml:
            return xml #get_poster(xml)
        return None

def thumb(image):
    try:
        orig = Image.open(image)
        for size in sizes:
            img = orig.resize(size, Image.ANTIALIAS)
            dur = image_dir + "x".join([str(s) for s in size]) + "/" + os.path.split(image)[-1]
            img.save(dur, quality=95)
            bukkit.upload(dur,'/' + "x".join([str(s) for s in size]) + '/' + os.path.split(dur)[-1])
            os.remove(dur)
    except IOError, e:
        print "IOError:", str(e)


def image():
    q = 'select show_id from show where image = False order by show_id asc;'
    q2 = 'select thetvdb_id, show_id from id_map where show_id = ANY (%s) order by show_id asc;'
    q = cur.mogrify(q)
    cur.execute(q)
    ids = cur.fetchall()
    z = [i for id in ids for i in id]
    q2 = cur.mogrify(q2,(z,))
    cur.execute(q2)
    tvdb_ids = cur.fetchall()
    for id in tvdb_ids:
        poster = get_poster(xml(id[0]))
        if poster:
            try:
                local = os.path.join(image_dir, str(id[1]) + os.path.splitext(poster)[-1])
                ext = os.path.splitext(poster)[-1]
                urllib.urlretrieve(poster, local)
                thumb(local)
                q = 'update show set image = True where show_id = %s;'
                q = cur.mogrify(q,(id[1],))
                cur.execute(q)
                conn.commit()
            except:
                raise

def image2():
    q = 'select show_id from show where image2 = False order by show_id asc;'
    q2 = 'select thetvdb_id, show_id from id_map where show_id = ANY (%s) order by show_id asc;'
    q = cur.mogrify(q)
    cur.execute(q)
    ids = cur.fetchall()
    z = [i for id in ids for i in id]
    q2 = cur.mogrify(q2,(z,))
    cur.execute(q2)
    tvdb_ids = cur.fetchall()
    for id in tvdb_ids:
        poster = get_fanart(xml(id[0]))
        if poster:
            dirp = image_dir + "detail/"
            local = os.path.join(dirp, str(id[1]) + os.path.splitext(poster)[-1])
            urllib.urlretrieve(poster, local)
            print id[1]
            try:
                orig = Image.open(local)
                orig.save(local, quality=95)
                bukkit.upload(local,'/detail/' + os.path.split(local)[-1])
                os.remove(local)
                q = 'update show set image2 = True where show_id = %s;'
                q = cur.mogrify(q,(id[1],))
                cur.execute(q)
                conn.commit()
            except:
                pass

def ohgorsh():
    image()
    image2()


ohgorsh()
