#!/usr/bin/env python
from search.indexer import get_writer
from search.indexer import schema
from whoosh import index
from models.Show import Show

import psycopg2

index.create_in('/var/web/tvdb/index',schema)

def update():
    conn = psycopg2.connect('dbname=noti user=postgres')
    conn.set_client_encoding('UTF8')
    cur = conn.cursor()
    q = "select show_id from show where hidden is False and status = 'Continuing';"
    cur.execute(q)
    res = cur.fetchall()
    shows = []
    for show in res:
        shows.append(Show(show[0], db=conn))
    writer = get_writer()

    for show in shows:
        if not show.rank:
            show.rank = 0
        if not show.description:
            show.description = ''
        writer.update_document(
            show_id = unicode(str(show.show_id), errors='ignore'),
            name = unicode(str(show.name), errors='ignore'),
            description = unicode(str(show.description), errors='ignore'),
            rank = unicode(str(show.rank), errors='ignore'))

    writer.commit()
    cur.close()
    conn.close()
    
    
if __name__ == "__main__":
    update()
