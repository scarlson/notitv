#!/usr/bin/env python
import psycopg2
import urllib2
import datetime
import time
from xml.etree import ElementTree

the_tv_db_api_key = '17F2768FC31BE4AE'
# http://www.thetvdb.com/api/17F2768FC31BE4AE/episodes/370780/en.xml
# http://www.thetvdb.com/api/17F2768FC31BE4AE/series/252480/all/en.xml

conn = psycopg2.connect('dbname=noti user=postgres')
conn.set_client_encoding('UTF8')
cur = conn.cursor()

q = cur.mogrify('select max(time) from tvdb_updates;')
cur.execute(q)
last_time = int(cur.fetchone()[0])
today = datetime.date.today()

new_shows = 0
new_eps = 0
alter_shows = 0
alter_eps = 0


def read(url):
    try:
        return urllib2.urlopen(url).read()
    except urllib2.URLError:
        print url
        return None
    except urllib2.HTTPError:
        print url
        return None


def qry(query):
    try:
        time.sleep(2)
        cur.execute(query)
        conn.commit()
        return True
    except:
        conn.rollback()
        return False


def update_map_id(series_xml):
    show = series_xml.find('Series')
    if show.find('SeriesName').text:
        s = {}
        s['id'] = show.find('id').text
        s['imdb_id'] = show.find('IMDB_ID').text
        s['zap2it_id'] = show.find('zap2it_id').text
        s['name'] = show.find('SeriesName').text
        while s['name'][-1] == ")":
            s['name'] = " ".join(s['name'].split()[:-1])
        query = cur.mogrify("select show_id from show where name like %s;", (s['name'],))
        cur.execute(query)
        try:
            show = cur.fetchone()[0]
        except:
            show = None
        print show
        query = cur.mogrify('select thetvdb_id, imdb_id, zap2it_id from id_map where show_id = %s;', (show,))
        cur.execute(query)
        res = cur.fetchone()
        if not res:
            try:
                query = cur.mogrify("insert into id_map (show_id, thetvdb_id, imdb_id, zap2it_id) values (%s,%s,%s,%s);", (show, s['id'], s['imdb_id'], s['zap2it_id']))
                qry(query)
            except:
                raise
        if res:
            if (res[0] != s['id']) or (res[1] != s['imdb_id']) or (res[2] != s['zap2it_id']):
                try:
                    query = cur.mogrify('update id_map set thetvdb_id = %s, imdb_id = %s, zap2it_id = %s where show_id = %s;', (s['id'], s['imdb_id'], s['zap2it_id'], show))
                    qry(query)
                except:
                    raise


def update_show(series_xml):
    show = series_xml.find('Series')
    q = cur.mogrify('select show_id from id_map where thetvdb_id = %s;', (show.find('id').text,))
    cur.execute(q)
    try:
        show_id = cur.fetchone()[0]
    except:
        show_id = None
    name = show.find('SeriesName').text
    while name[-1] == ")":
        name = " ".join(name.split()[:-1])
    start_date = show.find('FirstAired').text
    if start_date == '0000-00-00':
        start_date = None
    description = show.find('Overview').text
    network = show.find('Network').text
    rank = show.find('Rating').text
    status = show.find('Status').text
    people = show.find('Actors').text
    genre = show.find('Genre').text
    rating = show.find('ContentRating').text
    dow = show.find('Airs_DayOfWeek').text
    time = show.find('Airs_Time').text

    if show_id and network and start_date:  # show exists, update
        q = cur.mogrify('select * from show where show_id = %s;', (show_id,))
        cur.execute(q)
        q = cur.fetchone()
        if q[2] != start_date or q[3] != description or q[4] != network or q[5] != rank or q[6] != status or q[7] != people or q[8] != genre or q[9] != rating:
            try:
                query = cur.mogrify('update show set start_date = %s, description = %s, network = %s, rank = %s, status = %s, people = %s, genre = %s, rating = %s, dow = %s, time = %s where show_id = %s;', (start_date, description, network, rank, status, people, genre, rating, dow, time, show_id))
                if qry(query):
                    global alter_shows
                    alter_shows += 1
            except:
                pass
    elif network and start_date and not show_id:  # show doesn't exist, insert
        try:
            query = cur.mogrify('insert into show (name, start_date, description, network, rank, status, people, genre, rating, dow, time) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);', (name, start_date, description, network, rank, status, people, genre, rating, dow, time))
            if qry(query):
                global new_shows
                new_shows += 1
        except:
            pass


def update_episodes(series_xml):
    if series_xml.find('Series'):
        seriesid = series_xml.find('Series').find('id').text
    else:
        seriesid = series_xml.find('Episode').find('seriesid').text
    q = cur.mogrify('select show_id from id_map where thetvdb_id = %s;', (seriesid,))
    cur.execute(q)
    try:
        show_id = cur.fetchone()[0]
    except:
        show_id = None
    if show_id:
        for ep in series_xml:
            if ep.tag == "Episode":
                s = {}
                s['name'] = ep.find('EpisodeName').text
                s['desc'] = ep.find('Overview').text
                s['air_date'] = ep.find('FirstAired').text
                if s['air_date']:
                    s['air_date'] = datetime.datetime.strptime(s['air_date'], '%Y-%m-%d').date()
                else:
                    s['air_date'] = datetime.date(1900, 01, 01)
                s['se_no'] = str(int(float(ep.find('SeasonNumber').text)))
                s['ep_no'] = str(int(float(ep.find('EpisodeNumber').text)))
                s['imdb_id'] = ep.find('IMDB_ID').text
                if ((s['name']) and (s['air_date'] >= today)):
                    q = cur.mogrify('select episode_id from episode where episode_no = %s and season_no = %s and show_id = %s;', (s['ep_no'], s['se_no'], show_id))
                    cur.execute(q)
                    try:
                        q = cur.fetchone()[0]
                    except:
                        q = None
                    if q:
                        query = cur.mogrify("update episode set name = %s, description = %s, air_date = %s, imdb_id = %s where episode_id = %s;", (s['name'], s['desc'], s['air_date'], s['imdb_id'], q))
                        if qry(query):
                            global alter_eps
                            alter_eps += 1
                    else:
                        query = cur.mogrify("insert into episode (show_id, name, description, air_date, episode_no, season_no, imdb_id) values (%s,%s,%s,%s,%s,%s,%s);", (show_id, s['name'], s['desc'], s['air_date'], s['ep_no'], s['se_no'], s['imdb_id']))
                        if qry(query):
                            global new_eps
                            new_eps += 1


def update_episode(ep_id):
    url = 'http://www.thetvdb.com/api/' + the_tv_db_api_key + '/episodes/' + str(ep_id) + '/en.xml'
    ep = read(url)
    if ep:
        try:
            xml = ElementTree.fromstring(ep)
            xml = ElementTree.ElementTree(xml).getroot()
        except:
            xml = None
        if xml:
            update_episodes(xml)


def full_show(show_id):
    url = 'http://www.thetvdb.com/api/' + the_tv_db_api_key + '/series/' + str(show_id) + '/en.xml'
    show = read(url)
    if show:
        try:
            xml = ElementTree.fromstring(show)
            xml = ElementTree.ElementTree(xml).getroot()
        except:
            print "XML not found"
            xml = None
        if xml:
            if (xml.find('Series').find('SeriesName').text is not None):
                show = xml.find('Series')
                tvdb_id = show.find('id').text
                q = cur.mogrify('select show_id from id_map where thetvdb_id = %s;', (tvdb_id,))
                cur.execute(q)
                try:
                    show_id = cur.fetchone()[0]
                except:
                    print "show_id not found"
                    show_id = None

                status = None
                if show_id:
                    q = cur.mogrify('select status from show where show_id = %s;', (show_id,))
                    cur.execute(q)
                    status = cur.fetchone()[0]

                flag = 0
                if status:
                    if (status == 'Continuing' and xml.find('Series').find('Status').text in ['Ended', None]):
                        flag = 1

                if xml.find('Series').find('Status').text == 'Continuing' or flag:
                    url = 'http://www.thetvdb.com/api/' + the_tv_db_api_key + '/series/' + str(tvdb_id) + '/all/en.xml'
                    show = read(url)
                    try:
                        xml = ElementTree.fromstring(show)
                        xml = ElementTree.ElementTree(xml).getroot()
                    except:
                        xml = None
                    if xml:
                        update_show(xml)
                        update_map_id(xml)
                        update_episodes(xml)


def derp():
    url = 'http://www.thetvdb.com/api/' + the_tv_db_api_key + '/updates/updates_day.xml'
    results = read(url)
    if results:
        xml = ElementTree.fromstring(results)
        xml = ElementTree.ElementTree(xml).getroot()
        longest = 0
        for shit in xml:
            try:
                if int(shit.find('time').text) >= int(last_time):
                    if shit.tag == 'Series':
                        q = cur.mogrify('select show_id from id_map where thetvdb_id = %s;', (shit.find('id').text,))
                        cur.execute(q)
                        q = cur.fetchone()
                        if q:
                            full_show(shit.find('id').text)
                        #else:  # full show info go
                        #    try:
                        #        full_show(shit.find('id').text)
                        #    except:
                        #        pass
                        if int(shit.find('time').text) > longest:
                            longest = int(shit.find('time').text)
                    elif shit.tag == 'Episode':
                        #episode.update()
                        q = cur.mogrify('select show_id from id_map where thetvdb_id = %s;', (shit.find('Series').text,))
                        if q:  # update only, fff
                            update_episode(shit.find('id').text)
                        #else:  # full show info go
                        #    full_show(shit.find('Series').text)
                        if int(shit.find('time').text) > longest:
                            longest = int(shit.find('time').text)
                    else:  #banners.........
                        pass
            except:
                raise

    if longest > last_time:
        now = datetime.datetime.now()
        q = cur.mogrify('insert into tvdb_updates (time, update_time, show_adds, show_alters, episode_adds, episode_alters) values (%s, %s, %s, %s, %s, %s);', (longest, now, new_shows, alter_shows, new_eps, alter_eps))
        qry(q)


if __name__ == "__main__":
    derp()
