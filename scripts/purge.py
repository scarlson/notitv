#!/usr/bin/env python
import psycopg2
import datetime

conn = psycopg2.connect('dbname=noti user=postgres')
conn.set_client_encoding('UTF8')
cur = conn.cursor()
today = datetime.date.today()


def purge():
    query = "delete from episode where air_date::date < %s;"
    q = cur.mogrify(query, (today,))
    cur.execute(q)
    query = "delete from episode where air_date is NULL;"
    cur.execute(query)
    try:
        conn.commit()
    except:
        conn.rollback()
    old_isolation_level = conn.isolation_level
    conn.set_isolation_level(0)
    q = "vacuum;"
    cur.execute(q)
    conn.set_isolation_level(old_isolation_level)
    cur.close()
    conn.close()
        
        
if __name__ == "__main__":
    purge()
