#!/usr/bin/env python
import psycopg2
import urllib2
from xml.etree import ElementTree
from models.Show import Show

the_tv_db_api_key = '17F2768FC31BE4AE'
# http://www.thetvdb.com/api/17F2768FC31BE4AE/episodes/370780/en.xml
# http://www.thetvdb.com/api/17F2768FC31BE4AE/series/252480/all/en.xml

conn = psycopg2.connect('dbname=noti user=postgres')
conn.set_client_encoding('UTF8')
cur = conn.cursor()

def read(url):
    try:
        return urllib2.urlopen(url).read()
    except urllib2.URLError:
        return None
    except urllib2.HTTPError:
        return None

def get_status(tvdb_id):
    url = 'http://www.thetvdb.com/api/' + the_tv_db_api_key + '/series/' + str(tvdb_id) + '/en.xml'
    show = read(url)
    status = None
    if show:
        try:
            xml = ElementTree.fromstring(show)
            xml = ElementTree.ElementTree(xml).getroot()
        except:
            xml = None
        if xml:
            if (xml.find('Series').find('SeriesName').text is not None):
                show = xml.find('Series')
                status = show.find('Status').text
    return status

def derp():
        query = cur.mogrify('select thetvdb_id, show_id from id_map;')
        cur.execute(query)
        res = cur.fetchall()
        for show in res:
            s = Show(show[1],db=conn)
            tvdb_status = get_status(show[0])
            if tvdb_status != 'Continuing':
                s.delete()

if __name__ == "__main__":
    derp()
