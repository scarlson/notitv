from models.Base2 import Base
from models.Emailer import Emailer
import random
import datetime

spamtime = datetime.timedelta(minutes=2)


class Email(Base):
    def unique_hash(self):
        _hashpool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        _hash = "".join(random.choice(_hashpool) for i in range(8))
        existing = 'select distinct hash_sub from email;'
        existing = self.select(existing) or ""
        if _hash in existing:
            return self.unique_hash()
        return _hash

    def save(self):
        if not self.hash_sub:
            self.hash_sub = self.unique_hash()
        q = 'select user_id from email where user_id = %s;'
        q = self.select(q, self.user_id)
        if q:
            self.hash_sub = self.unique_hash()
            query = """update email set email = %s, hash_sub = %s, time = %s
                where user_id = %s;"""
            params = (self.email, self.hash_sub, self.time, self.user_id)
            return self.update(query, *params)
        else:
            query = """insert into email values (%s, %s, %s, %s);"""
            params = (self.user_id, self.email, self.hash_sub, self.time)
            return self.update(query, *params)
        return False

    def delete(self):
        q = "select * from email where user_id = %s;"
        q = self.select(q, self.user_id)
        if q:
            q = "delete from email where user_id = %s;"
            return self.update(q, self.user_id)
        return True

    def validate(self):
        if not self.time:
            self.time = datetime.datetime.now()
            if not self.save():
                return False
        elif self.time + spamtime < datetime.datetime.now():
            self.time = datetime.datetime.now()
            if not self.save():
                return False
        else:
            return False
        recipient = self.email
        subject = "Email address verification"
        body = "A change of address was requested for this email on Noti.tv." + \
            "  The link below will activate this address.\n\n" + \
            "http://noti.tv/u/email/" + self.hash_sub
        emails = Emailer(recipient, subject, body)
        emails.send()
        return True
