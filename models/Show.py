import datetime
from models.Episode import Episode
from models.Base2 import Base


class Show(Base):
    def __eq__(self, notself):
        if isinstance(notself, Show):
            return self.show_id == notself.show_id
        return 0

    @property
    def next_episode(self):
        if not hasattr(self, '_next_episode'):
            self._next_episode = None
            if self.status == 'Continuing':
                today = datetime.date.today()                
                query = """select episode_id from episode where
                    air_date::date >= %s and show_id = %s order by air_date asc limit 1;"""
                params = (today, self.show_id)                
                res = self.select(query, *params)
                if res:
                    if isinstance(res[0],tuple):
                        res = res[0]
                    self._next_episode = Episode(res[0], db = self._db)
        return self._next_episode

    @property
    def episodes(self):
        if not hasattr(self, '_episodes'):
            query = '''select episode_id from episode where show_id = %s order by
            season_no::int asc, episode_no::int asc;'''
            params = self.show_id
            results = self.select(query, params)
            self._episodes = []
            if results:
                if isinstance(results[0], tuple):
                    for result in results:
                        self._episodes.append(Episode(result[0], db = self._db))
                elif isinstance(results[0], int):
                    self._episodes.append(Episode(results[0], db = self._db))
        return self._episodes

    def toggle_hidden(self):
        query = '''update show set hidden = %s where show_id = %s;'''
        params = (not self.hidden, self.show_id)
        if self.update(query, *params):
            self.hidden = not self.hidden
            return True
        else:
            return False

    def delete(self):
        if not self.show_id:
            return False
        query_ep = "delete from episode where show_id = %s;"
        query_idmap = "delete from id_map where show_id = %s;"
        query_subs = "delete from show_subs where show_id = %s;"
        query_show = "delete from show where show_id = %s;"
        eps = self.update(query_ep, self.show_id)
        idmap = self.update(query_idmap, self.show_id)
        subs = self.update(query_subs, self.show_id)
        show = self.update(query_show, self.show_id)
        return (eps, idmap, subs, show)
