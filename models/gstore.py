#!/usr/bin/python
import StringIO
import os
import boto
from oauth2_plugin import oauth2_plugin

# install GSUTIL
#   https://developers.google.com/storage/docs/gsutil_install
#   Add next line to ~/.bashrc
#       export PYTHONPATH=${PYTHONPATH}:$HOME/gsutil/boto:$HOME/gsutil
#   gsutil config


# URI scheme for Google Cloud Storage.
GOOGLE_STORAGE = 'gs'
# URI scheme for accessing local files.
LOCAL_FILE = 'file'

RW_SCOPE = 'https://www.googleapis.com/auth/devstorage.read_write'
RO_SCOPE = 'https://www.googleapis.com/auth/devstorage.read_only'

image_dir = "/var/web/tvdbimages/"
public_dir = "http://images.noti.tv/" + "bucket" + "/" + "object"

PROJECT_ID = "826229619647"
default_bucket = "images.noti.tv"
ACL = "public-read"

class Bucket(object):
    def __init__(self, name=default_bucket):
        self.name = name
        self.uri = boto.storage_uri(name, GOOGLE_STORAGE)

    def public_read(self):
        bukkit = boto.storage_uri(default_bucket, GOOGLE_STORAGE)
        bukkit.set_acl(ACL)

    @property
    def contents(self):
        if not hasattr(self, '_contents'):
            self._contents = self.uri.get_bucket()
        return self._contents

    def upload(self, source, destination):
        data = file(source, 'r')
        dst_uri = boto.storage_uri(
            default_bucket + destination, GOOGLE_STORAGE)
        
        dst_uri.new_key().set_contents_from_file(data)
        dst_uri.set_acl(ACL)
        data.close()

    def derp(self):
        for obj in self.contents:
            obj.set_acl(ACL)

