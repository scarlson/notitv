import psycopg2


class Base(object):
    @property
    def conn(self):
        if not hasattr(self, '_conn'):
            if self.db:
                self._conn = self.db
            else:
                self._conn = psycopg2.connect('dbname=noti user=postgres')
                self._conn.set_client_encoding('UTF8')
        return self._conn

    @property
    def cur(self):
        if not hasattr(self, '_cur'):
            self._cur = self.conn.cursor()
        if self._cur.closed:
            self._cur = self.conn.cursor()            
        return self._cur

    def select(self, query, *params):
        if params:
            query = self.cur.mogrify(query, params)
        else:
            query = self.cur.mogrify(query)            
        self.cur.execute(query)
        res = self.cur.fetchall()
        self.cur.close()
        if res:
            if len(res) > 1:
                return res
            else:
                return res[0]

    def update(self, query, *params):
        if params:
            query = self.cur.mogrify(query, params)
        else:
            query = self.cur.mogrify(query)            
        try:
            self.cur.execute(query)
            self.conn.commit()
            self.cur.close()
            return True
        except:
            self.conn.rollback()
            self.cur.close()
            return False

