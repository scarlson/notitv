from models.Show import Show
from models.Base2 import Base
import bcrypt


class User(Base):
    def __init__(self, value, key=None, db=None):
        self._table = "users"
        self._key = key or 'user_id'
        self._db = db
        self._getVals()
        self._fetch(value)

    @property
    def subs(self):
        if not hasattr(self, '_subs'):
            query = 'select distinct show_id from show_subs where user_id = %s;'
            results = self.select(query, self.user_id)
            self._subs = []
            if results:
                for res in results:
                    try:
                        s = Show(res[0], db=self._db)
                    except:
                        s = Show(res, db=self._db)
                    self._subs.append(s)
        return self._subs

    @property
    def exists(self):
        query = None
        params = None
        res = None

        if self.user_id:
            return True
        elif self.source == 'twitter':
            query = "select distinct * from users where twitter LIKE %s;"
            params = self.twitter
        elif self.source == 'facebook':
            query = "select distinct * from users where facebook LIKE %s;"
            params = self.facebook
        elif self.source == 'google':
            query = "select distinct * from users where google LIKE %s;"
            params = self.google
        elif self.source == 'local':
            query = """select distinct * from users where username LIKE %s
            and source='local';"""
            params = self.username

        if query and params:
            res = self.select(query, params)

        if res:
            return True
        return False

    def set_password(self, password):
        self.hash = bcrypt.hashpw(password, bcrypt.gensalt())

    def check_password(self, password):
        return self.hash == bcrypt.hashpw(password, self.hash)

    def load_by_email(self, email):
        query = """select distinct * from users where email LIKE %s
        and source='local';"""
        result = self.select(query, email)
        if result:
            self.__init__(*result)
            return True
        return False

    def load_by_username(self, username):
        query = """select distinct * from users where username LIKE %s
        and source='local';"""
        result = self.select(query, username)
        if result:
            self.__init__(*result)
            return True
        return False

    def delete(self):
        query = None
        params = None
        if not self.user_id:
            return None
        if self.source == 'local':
            query = """delete from users where username LIKE %s
            and source='local';"""
            params = self.username
        if self.source == 'twitter':
            query = "delete from users where twitter LIKE %s;"
            params = self.twitter
        if self.source == 'facebook':
            query = "delete from users where facebook LIKE %s;"
            params = self.facebook
        if self.source == 'google':
            query = "delete from users where google LIKE %s;"
            params = self.google

        query2 = "delete from show_subs where user_id = %s;"
        params2 = self.user_id
        query3 = "delete from email where user_id = %s;"
        params3 = self.user_id
        query4 = "delete from unsub where user_id = %s;"
        params4 = self.user_id

        if query:
            self.update(query4, params4)
            self.update(query3, params3)
            self.update(query2, params2)
            self.update(query, params)
            return True
        return False

    def unsubhash(self):
        q = "select hash from unsub where user_id = %s;"
        params = self.user_id
        hashes = self.select(q, params)
        if hashes:
            return hashes[0]

        #make hash, save it.
        import random
        while 1:
            _hashpool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
            _hash = "".join(random.choice(_hashpool) for i in range(8))
            existing = 'select distinct hash_sub from email;'
            existing = self.select(existing) or ""
            if _hash not in existing:
                break

        q = "insert into unsub values (%s, %s);"
        params = (self.user_id, _hash)
        if self.update(q, params):
            return _hash
        return None
