from handlers.BaseHandler import BaseHandler
import tornado.web


class SearchHandler(BaseHandler):
    @tornado.web.asynchronous
    def get(self):
        from search.views import search
        try:
            q = str(self.request.arguments['q'][0]).lower()
        except:
            q = None
        if q:
            res = search(q)
            shows = []
            if res:
                for show in res:
                    if self.get_show(show['show_id']):
                        shows.append(self.get_show(show['show_id']))
            data = {}
            data['user'] = self.get_current_user()
            data['subs'] = self.get_current_user_subs()
            data['shows'] = shows
            if len(data['shows']) == 1:
                self.redirect('/s/' + str(shows[0].show_id))
            self.render("searchresults.html", **data)
        else:
            self.redir()


class ShowHandler(BaseHandler):
    @tornado.web.asynchronous
    def get(self, show):
        try:
            showid = int(show)
        except:
            showid = None
        data = {}
        data['user'] = self.get_current_user()
        data['subs'] = self.get_current_user_subs()
        data['show'] = None
        if showid:
            from models.Idmap import Idmap
            data['idmap'] = Idmap(showid, key="show_id", table="id_map")
            if self.get_show(showid):
                show = self.get_show(showid)
                data['show'] = show
        data['affiliate'] = self.affiliate(data['show'].name)
        if data['show'] and data['show']._exists:
            self.render("showdetail.html", **data)
        else:
            raise tornado.web.HTTPError(404)


class AllCalendarHandler(BaseHandler):
    def get(self):
        import datetime
        data = {}
        data['today'] = datetime.date.today()
        data['user'] = self.get_current_user()
        data['dates'] = {}
        for show_id in self.data['shows']:
            show = self.data['shows'][show_id]['show']
            if show.next_episode:
                try:
                    data['dates'][show.next_episode.air_date].append(show)
                except KeyError:
                    data['dates'][show.next_episode.air_date] = []
                    data['dates'][show.next_episode.air_date].append(show)
        data['sorted'] = sorted([key for key in data['dates']])
        self.render("calendar.html", **data)
