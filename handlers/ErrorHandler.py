import datetime
from handlers.BaseHandler import BaseHandler


class FourOhFourHandler(BaseHandler):
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        now = datetime.datetime.now()
        year = "%d" % now.year
        day = str(now.timetuple().tm_yday)
        data['stardate'] = ".".join([year, day])
        self.render('404.html', **data)
