from handlers.BaseHandler import BaseHandler
from models.Show import Show
from urllib import quote, unquote
import tornado.web

sizes = ((170, 250), (272, 399))
image_dir = '/var/web/tvdbimages/'


class ShowEditHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, show_id):
        data = {}
        data['show'] = Show(show_id)
        data['user'] = self.get_current_user()
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            self.render('show_edit.html', **data)

    @tornado.web.authenticated
    def post(self, show_id):
        user = self.get_current_user()
        if user.role < 2:
            raise tornado.web.HTTPError(403)
        else:
            orig_show = Show(show_id)
            name = self.request.arguments['name'][0]
            desc = self.request.arguments['desc'][0]
            network = self.request.arguments['network'][0]
            status = self.request.arguments['status'][0]
            dow = self.request.arguments['dow'][0]
            time = self.request.arguments['time'][0]
            #image = self.request.arguments['image'][0]
            orig_show.name = name if name else orig_show.name
            orig_show.description = desc if desc else orig_show.description
            orig_show.network = network if network else orig_show.network
            orig_show.status = status if status else orig_show.status
            orig_show.dow = dow if dow else orig_show.dow
            orig_show.time = time if time else orig_show.time
            #orig_show.image = image if image else orig_show.image
            orig_show.save()
            self.recache_show(orig_show.show_id)
            self.redirect('/s/' + str(show_id))


class ShowImageHandler(BaseHandler):
    def thumb(self, image):
        import Image
        import os
        from models.gstore import Bucket
        bukkit = Bucket()
        try:
            orig = Image.open(image)
            orim = image_dir + "original/" + os.path.split(image)[-1]
            orig.save(orim, quality=95)
            bukkit.upload(orim, '/original/' + os.path.split(image)[-1])
            for size in sizes:
                img = orig.resize(size, Image.ANTIALIAS)
                local = image_dir + "x".join([str(s) for s in size]) + "/" \
                    + os.path.split(image)[-1]
                img.save(local, quality=95)
                bukkit.upload(local, '/' + "x".join([str(s) for s in size])
                              + '/' + os.path.split(image)[-1])
                os.remove(local)
            os.remove(orim)
            return True
        except IOError:
            raise
            return False

    @tornado.web.authenticated
    def post(self, show_id):
        user = self.get_current_user()
        if user.role < 2:
            raise tornado.web.HTTPError(403)
        else:
            show = Show(show_id)
            import urllib
            import os
            image_url = self.request.arguments['image'][0]
            local = os.path.join(image_dir, str(show_id) + os.path.splitext(image_url)[-1])
            urllib.urlretrieve(image_url, local)
            if self.thumb(local):
                show.image = True
                show.save()
            os.remove(local)
            self.recache_show(str(show_id))
            self.redirect('/s/' + str(show_id))


class ShowImage2Handler(BaseHandler):
    def thumb(self, image):
        import Image
        import os
        from models.gstore import Bucket
        bukkit = Bucket()
        try:
            orig = Image.open(image)
            orim = image_dir + "detail/" + os.path.split(image)[-1]
            orig.save(orim, quality=95)
            bukkit.upload(orim, '/detail/' + os.path.split(image)[-1])
            os.remove(orim)
            return True
        except IOError:
            raise
            return False

    @tornado.web.authenticated
    def post(self, show_id):
        user = self.get_current_user()
        if user.role < 2:
            raise tornado.web.HTTPError(403)
        else:
            show = Show(show_id)
            import urllib
            import os
            image_url = self.request.arguments['image2'][0]
            local = os.path.join(image_dir, str(show_id) + os.path.splitext(image_url)[-1])
            urllib.urlretrieve(image_url, local)
            if self.thumb(local):
                show.image2 = True
                show.save()
            os.remove(local)
            self.recache_show(str(show_id))
            self.redirect('/s/' + str(show_id))


class ShowDeleteHandler(BaseHandler):
    def get(self, show_id):
        user = self.get_current_user()
        if user.role < 2:
            raise tornado.web.HTTPError(403)
        else:
            show = Show(show_id)
            if show.delete():
                try:
                    del self.data['shows'][show_id]
                except KeyError:
                    pass
                self.redirect('/mod/list')
            else:
                self.redirect('/s/' + str(show_id))


class ToggleHandler(BaseHandler):
    @tornado.web.authenticated
    @tornado.web.asynchronous
    def get(self, show_id):
        user = self.get_current_user()
        if user.role < 2:
            raise tornado.web.HTTPError(403)
        else:
            _show = Show(show_id)
            self.write({'success': self._toggle(_show), 'val': _show.hidden})
            self.finish()

    def _toggle(self, show):
        try:
            return show.toggle_hidden()
        except:
            return False


class ListHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        user = self.get_current_user()
        if user.role < 2:
            raise tornado.web.HTTPError(403)
        else:
            data = {}
            data['user'] = self.get_current_user()
            data['hidden'] = []
            data['images'] = []
            data['images2'] = []
            data['dow'] = []
            data['time'] = []
            cur = self.db.cursor()
            query = """select count(show_id) from show where hidden = true;"""
            cur.execute(query)
            data['hidden'] = cur.fetchall()[0][0]

            query = """select count(show_id) from show where image = false;"""
            cur.execute(query)
            data['images'] = cur.fetchall()[0][0]

            query = """select count(show_id) from show where image2 = false;"""
            cur.execute(query)
            data['images2'] = cur.fetchall()[0][0]

            query = """select count(show_id) from show where dow is NULL;"""
            cur.execute(query)
            data['dow'] = cur.fetchall()[0][0]

            query = """select count(show_id) from show where time is NULL;"""
            cur.execute(query)
            data['time'] = cur.fetchall()[0][0]

            query = """select count(show_id) from show where description is NULL;"""
            cur.execute(query)
            data['desc'] = cur.fetchall()[0][0]

            query = """select count(distinct network) from show;"""
            cur.execute(query)
            data['networks'] = cur.fetchall()[0][0]

            query = """select count(*) from users;"""
            cur.execute(query)
            data['users'] = cur.fetchall()[0][0]

            self.render('mod_list.html', **data)
            #self.render('data_check.html', data=data)


class ImagesHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        data['shows'] = []
        data['type'] = 'Shows missing images'
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            cur = self.db.cursor()
            query = "select show_id from show where image = false;"
            cur.execute(query)
            shows = cur.fetchall()
            for show in shows:
                data['shows'].append(Show(show[0], db=self.db))
            self.render('mod.html', **data)
            #self.render('data_check.html', data=data)


class Images2Handler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        data['shows'] = []
        data['type'] = 'Shows missing images2'
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            cur = self.db.cursor()
            query = "select * from show where image2 = false;"
            cur.execute(query)
            shows = cur.fetchall()
            for show in shows:
                data['shows'].append(Show(show[0], db=self.db))
            self.render('mod.html', **data)
            #self.render('data_check.html', data=data)


class TimeHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        data['shows'] = []
        data['type'] = 'Shows missing time'
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            cur = self.db.cursor()
            query = "select * from show where time is NULL;"
            cur.execute(query)
            shows = cur.fetchall()
            for show in shows:
                data['shows'].append(Show(show[0], db=self.db))
            self.render('mod.html', **data)
            #self.render('data_check.html', data=data)


class DowHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        data['shows'] = []
        data['type'] = 'Shows missing DOW'
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            cur = self.db.cursor()
            query = "select * from show where dow is NULL;"
            cur.execute(query)
            shows = cur.fetchall()
            for show in shows:
                data['shows'].append(Show(show[0], db=self.db))
            self.render('mod.html', **data)
            #self.render('data_check.html', data=data)


class DescHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        data['shows'] = []
        data['type'] = 'Shows missing description'
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            cur = self.db.cursor()
            query = "select * from show where description is NULL;"
            cur.execute(query)
            shows = cur.fetchall()
            for show in shows:
                data['shows'].append(Show(show[0], db=self.db))
            self.render('mod.html', **data)
            #self.render('data_check.html', data=data)


class HiddenHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        data['shows'] = []
        data['type'] = 'Hidden shows'
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            cur = self.db.cursor()
            query = "select * from show where hidden = true;"
            cur.execute(query)
            shows = cur.fetchall()
            for show in shows:
                data['shows'].append(Show(show[0], db=self.db))
            self.render('mod.html', **data)
            #self.render('data_check.html', data=data)


class UsersHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            from models.User import User
            query = "select * from users;"
            users = self.select(query)
            data['users'] = [User(user[0]) for user in users]
            self.render('users.html', **data)


class UserHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, user_id):
        data = {}
        data['user'] = self.get_current_user()
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            from models.User import User
            query = "select * from users where user_id = %s;"
            user = self.select(query, (user_id,))
            data['u'] = User(user[0])
            self.render('user.html', **data)

    @tornado.web.authenticated
    def post(self, user_id):
        data = {}
        data['user'] = self.get_current_user()
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            from models.User import User
            username = self.request.arguments['username'][0]
            email = self.request.arguments['email'][0]
            role = self.request.arguments['role'][0]
            user = User(user_id)
            user.email = email or user.email
            user.username = username or user.username
            user.role = role or user.role
            user.save()
            self.redirect('/mod/user/' + user_id)


class UserDeleteHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, user_id):
        data = {}
        data['user'] = self.get_current_user()
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            from models.User import User
            user = User(user_id)
            if user.delete():
                self.redirect('/mod/users')
            else:
                self.redirect('/mod/user/' + user_id)


class ErrorsHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            from models.Error import Error
            q = "select error_id from error order by time desc limit 10;"
            data['errors'] = [Error(z) for z in self.select(q)]
            self.render('mod_errors.html', **data)


class ErrorViewHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, error_id):
        data = {}
        data['user'] = self.get_current_user()
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            from models.Error import Error
            data['error'] = Error(error_id)
            self.render('mod_error.html', **data)


class NetworksHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        data['networks'] = []
        data['type'] = 'Networks'
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            cur = self.db.cursor()
            query = "select distinct network from show order by network asc;"
            cur.execute(query)
            networks = cur.fetchall()
            for network in networks:
                if network:
                    data['networks'].append([network[0], quote(str(network[0]))])

            self.render('networks.html', **data)
            #self.render('data_check.html', data=data)


class NetworkHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, network=None):
        data = {}
        data['user'] = self.get_current_user()
        data['shows'] = []
        data['type'] = 'network'
        try:
            data['network'] = [network, quote(str(network))]
        except:
            data['network'] = [network, ]
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            if network:
                query = "select show_id from show where network = %s order by name asc;"
                shows = self.select(query, network)
            else:
                query = "select show_id from show where network is NULL;"
                shows = self.select(query)
            if shows:
                if len(shows) > 1:
                    for show in shows:
                        data['shows'].append(Show(show[0], db=self.db))
                else:
                    data['shows'].append(Show(shows[0], db=self.db))
                self.render('mod.html', **data)
                #self.render('data_check.html', data=data)
            else:
                self.redirect('/mod/networks')


class NetworkDeleteHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, network):
        #print 'DELETE', network
        network = unquote(network)
        data = {}
        data['shows'] = []
        data['user'] = self.get_current_user()
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            if network:
                query = "select show_id from show where network = %s;"
                shows = self.select(query, network)
            else:
                query = "select show_id from show where network is NULL;"
                shows = self.select(query)
            if len(shows) > 1:
                for show in shows:
                    data['shows'].append(Show(show[0], db=self.db))
            else:
                data['shows'].append(Show(shows[0], db=self.db))
            for show in data['shows']:
                show.delete()
            self.redirect('/mod/networks')


class ShowRefreshHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, show_id):
        data = {}
        data['user'] = self.get_current_user()
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            import scripts.tvdb_updates as TVDB
            cur = self.db.cursor()
            query = cur.mogrify('select thetvdb_id from id_map where show_id = %s;', (show_id,))
            cur.execute(query)
            res = cur.fetchone()
            #print 'Starting refresh'
            TVDB.full_show(res[0])
            self.redirect('/s/' + str(show_id))


class ShowAddHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, tvdb_id):
        data = {}
        data['user'] = self.get_current_user()
        if data['user'].role < 2:
            raise tornado.web.HTTPError(403)
        else:
            tvdb_id = str(tvdb_id)
            import scripts.tvdb_updates as TVDB
            TVDB.full_show(tvdb_id)
            cur = self.db.cursor()
            q = "select show_id from id_map where thetvdb_id = %s;"
            q = cur.mogrify(q, (tvdb_id,))
            cur.execute(q)
            q = cur.fetchone()
            if q:
                self.get_show(str(q[0]))
                self.redirect('/s/' + str(q[0]))
            else:
                self.redirect('/')
