from handlers.BaseHandler import BaseHandler
from models.Email import Email
from models.User import User
import tornado.web
import datetime
import re
email_reg = "^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$"


class AccountHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, errors=None, success=None):
        try:
            q = ["Anti-spam timeout, try again in " +
                 str(self.request.arguments['e'][0]).lower() +
                 " seconds."]
        except:
            q = None
        errors = errors or q
        data = {}
        data['user'] = self.get_current_user()
        data['email'] = Email(data['user'].user_id, key='user_id', db=self.db)
        data['errors'] = errors
        data['success'] = success
        if not data['user']:
            self.redir()
        else:
            data['subs'] = self.get_current_user_subs()
            #data['calendar'] = Calendar(data['subs']).cals
            #self.render("data_check.html", data=data)
            self.render("profile.html", **data)

    @tornado.web.authenticated
    def post(self):
        data = {}
        data['errors'] = []
        data['user'] = self.get_current_user()
        try:
            data['name'] = self.request.arguments['name'][0]
        except:
            data['name'] = None
        try:
            data['email'] = self.request.arguments['email'][0]
        except:
            data['email'] = None

        #self.render('data_check.html',data=data)
        if data['name']:
            data['user'].real_name = data['name']

        if data['email']:
            if (len(data['email']) < 7 or
                    re.match(email_reg, data['email']) is None):
                data['errors'].append('Invalid email.')

        emails = None
        if not data['errors']:
            #make sure data is valid before we make db requests
            cur = self.db.cursor()
            if data['email'] and data['email'] != data['user'].email:
                query = """select distinct * from users where lower(email)
                        LIKE %s;"""
                query = cur.mogrify(query, (data['email'].lower(),))
                cur.execute(query)
                emails = cur.fetchall()
                if not emails:
                    query = """select distinct * from email where lower(email)
                            LIKE %s;"""
                    query = cur.mogrify(query, (data['email'].lower(),))
                    cur.execute(query)
                    emails = cur.fetchall()
                if emails:
                    data['errors'].append('Email already exists.')

        if not data['errors']:
            if data['user'].save():
                self.set_secure_cookie('user', unicode(data['user'].user_id))
                if data['email'] and data['email'] != data['user'].email:
                    #this code needs to get the verification under way
                    # User changed email address, add new email to Email table and send accept hash
                    em = Email(data['user'].user_id, key='user_id', db=self.db)
                    em.email = data['email']
                    if em.save():
                        self.redirect('/u/email/validate')
                    else:
                        data['errors'].append("Email validation dun goofed.")
                else:
                    self.redirect('/account')
            else:
                data['errors'].append("There was an error saving your updates, please try again.")

        if data['errors']:
            self.get(data['errors'])


class NotificationHandler(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        data = {}
        data['user'] = self.get_current_user()
        try:
            data['notis'] = self.request.arguments['notis'][0]
        except:
            data['notis'] = None

        if data['notis']:
            data['user'].notifications = True
            data['user'].save()
        else:
            data['user'].notifications = False
            data['user'].save()
        self.redirect('/account')


class EmailValidateHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        #send emails to the user with the hash
        user = self.get_current_user()
        em = Email(user.user_id, key='user_id', db=self.db)
        try:
            if em.validate():
                self.redirect('/account')
            else:
                data = {}
                now = datetime.datetime.now()
                expire = em.time + datetime.timedelta(minutes=5)
                delta = expire - now
                delta = delta.seconds
                if expire > now:
                    data['errors'] = delta
                else:
                    data['errors'] = None
                self.redirect('/account?e='+str(data['errors']))
        except BaseException, e:
            print e
            self.redirect('/account')


class EmailSubHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, hashsub):
        #user clicked validate link in email
        #move email from email table to users table
        #delete email table entry
        data = {}
        em = Email(hashsub, key='hash_sub', db=self.db)
        data['user'] = User(em.user_id, db=self.db)
        data['user'].email = em.email
        if data['user'].save() and em.delete():
            if self.refresh_user():
                print "User refreshed!"
            else:
                print "User refresh failed!"
            self.set_secure_cookie('user', unicode(int(data['user'].user_id)))
        self.redirect('/account')


class EmailUnsubHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, hashunsub):
        #user clicked unsubscribe in email link, de-notification them
        data = {}
        cur = self.db.cursor()
        q = cur.mogrify("select user_id from unsub where hash = %s;", (hashunsub,))
        cur.execute(q)
        q = cur.fetchone()
        data['user'] = User(q[0], db=self.db)
        data['user'].notifications = False
        if data['user'].save():
            self.set_secure_cookie('user', unicode(int(data['user'].user_id)))
        self.redirect('/account')


class CalendarHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        data = {}
        data['today'] = datetime.date.today()

        data['user'] = self.get_current_user()
        data['subs'] = self.get_current_user_subs()
        data['dates'] = {}
        for show in data['subs']:
            if show.next_episode:
                try:
                    data['dates'][show.next_episode.air_date].append(show)
                except KeyError:
                    data['dates'][show.next_episode.air_date] = []
                    data['dates'][show.next_episode.air_date].append(show)
        data['sorted'] = sorted([key for key in data['dates']])
        self.render("calendar.html", **data)


class SubsHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        data['subs'] = self.get_current_user_subs()
        self.render("subs.html", **data)
