from handlers.BaseHandler import BaseHandler
#import logging
#logger = logging.getLogger('boilerplate.' + __name__)


class IndexHandler(BaseHandler):
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        self.save_loc('/')
        self.render("index.html", **data)


class FaqHandler(BaseHandler):
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        self.save_loc('/faq')
        self.render("faq.html", **data)


class AboutHandler(BaseHandler):
    def get(self):
        self.redirect('/faq')
