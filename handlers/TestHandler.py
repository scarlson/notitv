from handlers.BaseHandler import BaseHandler
from oauth2client.client import OAuth2WebServerFlow
from apiclient.discovery import build as build_service
import os
import httplib2


API_KEY = 'AIzaSyDpeRPiqPNpP5j2UfcIvsyHkeenIXrZLlY'
PROJECT_ID = '826229619647'
You = '00b4903a9766a367cdcd9f474cae27733950c60176b042879b136fbf76a1a4fb'
Owners = '00b4903a97bc1b0342a85060bba61f3fef861159bf75b2b01c35b1b008cb8afe'
Editors = '00b4903a97ec3b7ece98030ab89ebc715c7ea128ebbdb00b86df130bca9695ee'
Team = '00b4903a979603e18e543fc082be9b43fd82e0338a676419fb8328678e99a5ad'

SECRETS = "client_secrets.json"
CREDS = "credentials.json"

CLIENT_ID = "826229619647.apps.googleusercontent.com"
CLIENT_SECRET = "7ajEJtsD_wQnc-prBhUXy_ku"
REDIRECT_URI = "http://noti.tv/oauth2callback"
JavaScript_origins = "http://noti.tv"
USER_AGENT = "noti.tv"

RW_SCOPE = 'https://www.googleapis.com/auth/devstorage.read_write'
RO_SCOPE = 'https://www.googleapis.com/auth/devstorage.read_only'
image_dir = "/var/web/tvdbimages/"

FIELDS = 'items(name,media(timeCreated,hash,length))'


def GetBucketName(path):
  bucket = path[1:]  # Trim the preceding slash
  if bucket[-1] == '/':
    # Trim final slash, if necessary.
    bucket = bucket[:-1]
  return bucket


class TestHandler(BaseHandler):
  def get(self):
    flow = OAuth2WebServerFlow(client_id=CLIENT_ID,
                               client_secret=CLIENT_SECRET,
                               redirect_uri=REDIRECT_URI,
                               scope=RO_SCOPE,
                               access_type='online',
                               user_agent=USER_AGENT)

    print "path", self.request.path
    print "uri", self.request.uri
    bucket = GetBucketName(self.request.path)
    step2_url = flow.step1_get_authorize_url()
    # Add state to remember which bucket to list.
    self.redirect(step2_url + '&state=%s' % bucket)


class AuthHandler(BaseHandler):
  def get(self):
    flow = OAuth2WebServerFlow(client_id=CLIENT_ID,
                               client_secret=CLIENT_SECRET,
                               redirect_uri=REDIRECT_URI,
                               scope=RO_SCOPE,
                               user_agent=USER_AGENT)

    # Exchange the code (in self.request.params) for an access token.
    print "args", self.request.arguments
    # str(self.request.arguments['e'][0]).lower() + \
    credentials = flow.step2_exchange(self.request.arguments['code'][0])
    http = credentials.authorize(httplib2.Http())

    bucket = self.request.arguments['state'][0]
    storage = build_service('storage', 'v1beta1', http=http)
    list_response = storage.objects().list(bucket=bucket,fields=FIELDS).execute()
    data = {'items': list_response['items'], 'bucket_name': bucket}
    self.render('data_check.html',data=data)
