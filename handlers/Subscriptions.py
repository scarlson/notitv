from handlers.BaseHandler import BaseHandler
import tornado.web


class SubHandler(BaseHandler):
    @tornado.web.asynchronous
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        data['subs'] = self.get_current_user_subs()
        if not data['user']:
            self.write({'result': False})
            self.finish()
        else:
            try:
                show_id = int(self.request.arguments['id'][0])
            except:
                show_id = None

            if show_id and data['subs']:
                show = self.get_show(show_id)
                if show not in data['subs']:
                    self.refresh_user()
                    self.write({'result':
                        self._sub(data['user'].user_id, show_id)})
                    self.finish()
            elif show_id:
                self.refresh_user()
                self.write({'result': self._sub(data['user'].user_id, show_id)})
                self.finish()

    def _sub(self, user_id, show_id):
        try:
            conn = self.db
            cur = conn.cursor()
            query = """insert into show_subs (user_id, show_id)
            values (%s, %s);"""
            query = cur.mogrify(query, (user_id, show_id))
            cur.execute(query)
            conn.commit()
            return True
        except:
            return False


class UnsubHandler(BaseHandler):
    @tornado.web.asynchronous
    def get(self):
        data = {}
        data['user'] = self.get_current_user()
        data['subs'] = self.get_current_user_subs()
        if not data['user']:
            self.write({'result': False})
            self.finish()
        else:
            try:
                show_id = int(self.request.arguments['id'][0])
            except:
                show_id = None

            if data['subs'] and show_id:
                show = self.get_show(show_id)
                if show in data['subs']:
                    self.refresh_user()
                    self.write({'result':
                        self._unsub(data['user'].user_id, show_id)})
                    self.finish()

    def _unsub(self, user_id, show_id):
        try:
            conn = self.db
            cur = conn.cursor()
            query = """delete from show_subs where user_id = %s
            and show_id = %s;"""
            query = cur.mogrify(query, (user_id, show_id))
            cur.execute(query)
            conn.commit()
            return True
        except:
            return False
