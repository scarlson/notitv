import tornado.web
import datetime
import random
from models.User import User
from models.Show import Show
from urllib import quote, unquote


class BaseHandler(tornado.web.RequestHandler):
    def initialize(self, data=None):
        self.data = data

    def write_error(self, status, **kwargs):
        now = datetime.datetime.now()
        if 'exc_info' in kwargs:
            import traceback
            from models.Error import Error
            try:
                q = "select max(error_id) from error;"
                q = self.select(q)
                error_id = q[0] + 1 or 0
            except:
                error_id = 0
            exc_info = kwargs['exc_info']
            error = str(exc_info[0].__name__) + ": " + str(exc_info[1])
            trace_info = "".join(traceback.format_exception(*exc_info))
            e = Error(error_id)
            e.trace = trace_info
            e.error = str(error)
            e.code = status
            e.time = now
            e.request = repr(self.request)
            e.save()
        year = "%d" % now.year
        day = str(now.timetuple().tm_yday)
        data = {'error': status}
        data['user'] = self.get_current_user()
        data['stardate'] = ".".join([year, day])
        self.render(str(status) + ".html", **data)

    @property
    def db(self):
        if not hasattr(self, '_db'):
            if 'db' in self.data:
                self._db = self.data['db']
            else:
                pass
                #self._db = psycopg2.connect('dbname=noti user=postgres')
                #self._db.set_client_encoding('UTF8')
        return self._db

    def select(self, query, *params):
        cur = self.db.cursor()
        if params:
            query = cur.mogrify(query, params)
        else:
            query = cur.mogrify(query)
        cur.execute(query)
        res = cur.fetchall()
        cur.close()
        if res:
            if len(res) > 1:
                return res
            else:
                return res[0]

    def update(self, query, *params):
        cur = self.db.cursor()
        if params:
            query = cur.mogrify(query, params)
        else:
            query = cur.mogrify(query)
        try:
            cur.execute(query)
            self.db.commit()
            cur.close()
            return True
        except:
            self.db.rollback()
            cur.close()
            return False

    def refresh_user(self):
        print 'refreshing!'
        if self.get_secure_cookie("user"):
            try:
                user_id = int(self.get_secure_cookie("user"))
            except:
                self.clear_cookie('user')
                return None

            self.data['users'][user_id] = {}
            self.data['users'][user_id]['user'] = User(user_id, db=self.data['db'])
            self.data['users'][user_id]['time'] = datetime.datetime.now()
            return self.data['users'][user_id]['user']
        print 'cookie doesnt exist!'
        return None

    def get_current_user(self):
        if self.get_secure_cookie("user"):
            try:
                user_id = int(self.get_secure_cookie("user"))
            except:
                self.clear_cookie('user')
                return None

            try:
                _user = self.data['users'][user_id]
                hour = datetime.timedelta(hours=1)
                if _user['time'] + hour > datetime.datetime.now():
                    _user['time'] = datetime.datetime.now()
                else:
                    _user['user'] = User(user_id, db=self.data['db'])
                    _user['time'] = datetime.datetime.now()
                self.data['users'][user_id] = _user
                return _user['user']
            except KeyError:
                self.data['users'][user_id] = {}
                self.data['users'][user_id]['user'] = User(user_id, db=self.data['db'])
                self.data['users'][user_id]['time'] = datetime.datetime.now()
                return self.data['users'][user_id]['user']
        return None

    def get_current_user_subs(self):
        if self.get_current_user():
            return self.get_current_user().subs
        return None

    def logout_user(self):
        if self.get_current_user():
            del self.data['users'][self.get_current_user().user_id]
            self.clear_cookie('user')
            return True
        return False

    def recache_show(self, show_id):
        self.data['shows'][show_id] = {}
        self.data['shows'][show_id]['show'] = Show(show_id, db=self.db)
        self.data['shows'][show_id]['time'] = datetime.datetime.now()
        return self.data['shows'][show_id]['show']

    def get_show(self, show_id):
        if show_id in self.data['shows']:
            try:
                show = self.data['shows'][show_id]
                if show['time'] > datetime.datetime.now():
                    mins = random.randint(5, 15)
                    time = datetime.timedelta(minutes=mins)
                    show['time'] = datetime.datetime.now() + time
                else:
                    show['show'] = Show(show_id, db=self.db)
                    show['show'].db = self.data['db']
                    mins = random.randint(5, 15)
                    time = datetime.timedelta(minutes=mins)
                    show['time'] = datetime.datetime.now() + time
                self.data['shows'][show_id] = show
                return show['show']
            except KeyError:
                self.data['shows'][show_id] = {}
                self.data['shows'][show_id]['show'] = Show(show_id, db=self.db)
                mins = random.randint(5, 10)
                time = datetime.timedelta(minutes=mins)
                self.data['shows'][show_id]['time'] = datetime.datetime.now() + time
                return self.data['shows'][show_id]['show']
        else:
            try:
                self.data['shows'][show_id] = {}
                self.data['shows'][show_id]['show'] = Show(show_id, db=self.db)
                mins = random.randint(5, 10)
                time = datetime.timedelta(minutes=mins)
                self.data['shows'][show_id]['time'] = datetime.datetime.now() + time
                return self.data['shows'][show_id]['show']
            except KeyError:
                return None

    def affiliate(self, name):
        a = '<a type="amzn" search="' + str(name) + '">%s</a>'
        links = [
            'Behind on ' + str(name) + '?  Get the DVD box sets on Amazon.com.',
            'Like ' + str(name) + '?  Check out ' + str(name) + ' on Amazon.com.',
            'Get ' + str(name) + ' merchandise on Amazon.com.',
            '' + str(name) + ', streaming on Amazon Instant Video.',
            'Miss an episode?  Catch up on ' + str(name) + ' with Amazon Instant Video.',
            'Want to rewatch an episode?  Watch ' + str(name) + ' on Amazon Instant Video.',
        ]
        return a % links[random.randint(0, len(links)-1)]

    def get_user_ip(self):
        return self.request.remote_ip

    def save_loc(self, url):
        self.set_cookie('loc', quote(url))

    def get_loc(self):
        if self.get_cookie('loc'):
            return unquote(self.get_cookie('loc'))
        return None

    def redir(self):
        loc = self.get_loc()
        if loc:
            self.redirect(loc)
        else:
            self.redirect('/')
