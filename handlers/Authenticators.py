import tornado.web
import tornado.auth
import cPickle as pickle
from models.User import User
from models.Email import Email
from handlers.BaseHandler import BaseHandler


class WelcomeHandler(BaseHandler):
    def get(self):
        self.redirect('/')

    def post(self):
        try:
            _name = self.request.arguments['name'][0]
        except:
            _name = None
        try:
            _email = self.request.arguments['email'][0]
        except:
            _email = None
        try:
            _timezone = self.request.arguments['timezone'][0]
        except:
            _timezone = None
        try:
            _source = self.request.arguments['source'][0]
        except:
            _source = None

        user = None
        _user = None

        if self.get_secure_cookie('login'):
            user = pickle.loads(self.get_secure_cookie('login'))
            self.clear_cookie('login')

        if user:
            if _source == "facebook":
                _user = User(str(user['id']), key='facebook')
                _user.real_name = user['name']
                _user.email = user['email']
                _user.source = 'facebook'
            elif _source == "google":
                _user = User(user['google'], key='google')
                _user.real_name = user['name']
                _user.email = user['email']
                _user.source = 'google'
            elif _source == "twitter":
                _user = User(user['id_str'], key='twitter')
                _user.real_name = user['name']
                _user.username = '@' + str(user['username'])
                _user.source = 'twitter'

        flag = False
        if _user:
            _user.role = 1
            _user.notifications = True
            if _name:
                _user.name = _name
            if _email:
                if _email != _user.email:
                    flag = True
                else:
                    _user.email = _email
            if _timezone:
                _user.timezone = _timezone
            _user._key = 'user_id'
            if _user.save():
                if _source == "facebook":
                    _user = User(str(user['id']), key='facebook')
                elif _source == "google":
                    _user = User(user['google'], key='google')
                elif _source == "twitter":
                    _user = User(user['id_str'], key='twitter')
                if flag:
                    em = Email(_user.user_id, key="user_id", db=self.db)
                    em.email = _email
                    print em.__dict__
                    if em.save():
                        em.validate()
                self.set_secure_cookie('user', unicode(int(_user.user_id)))
        self.redir()
        #self.render("data_check.html",data=_user.__dict__)


class FacebookHandler(BaseHandler, tornado.auth.FacebookGraphMixin):
    @tornado.web.asynchronous
    def get(self):
        if self.get_argument("code", False):
            self.get_authenticated_user(
                redirect_uri='http://noti.tv/login/facebook',
                client_id=self.settings["facebook_api_key"],
                client_secret=self.settings["facebook_secret"],
                code=self.get_argument("code"),
                callback=self.async_callback(self._on_auth),
                extra_fields=['email', 'username'])
            return
        self.authorize_redirect(redirect_uri='http://noti.tv/login/facebook',
                                client_id=self.settings["facebook_api_key"],
                                extra_params={"scope": "email"})

    def _on_auth(self, user):
        #self.render("data_check.html",data=user)
        if not user:
            raise tornado.web.HTTPError(500, "Facebook auth failed")
        _user = User(str(user['id']), key='facebook', db=self.db)

        if _user.exists:
            self.set_secure_cookie('user', unicode(int(_user.user_id)))
            self.redir()
        else:
            data = {}
            data['user'] = None
            data['login'] = user
            data['source'] = 'facebook'
            self.set_secure_cookie('login', pickle.dumps(user))
            #self.render('data_check.html',data=user)
            self.render("welcome.html", **data)


class GoogleHandler(BaseHandler, tornado.auth.GoogleMixin):
    @tornado.web.asynchronous
    def get(self):
        if not self.get_current_user():
            if self.get_argument('openid.mode', None):
                self.get_authenticated_user(self.async_callback(self._on_auth))
                return
            self.authenticate_redirect()
        else:
            self.redir()

    def _on_auth(self, user):
        if not user:
            raise tornado.web.HTTPError(500, 'Google auth failed')
        try:
            user['google'] = user['username']
        except:
            user['google'] = user['email']
        _user = User(user['google'], key='google', db=self.db)

        if _user.exists:
            self.set_secure_cookie('user', unicode(int(_user.user_id)))
            self.redir()
        else:
            data = {}
            data['user'] = None
            data['login'] = user
            data['source'] = 'google'
            self.set_secure_cookie('login', pickle.dumps(user))
            self.render("welcome.html", **data)


class TwitterHandler(BaseHandler, tornado.auth.TwitterMixin):
    @tornado.web.asynchronous
    def get(self):
        if self.get_argument("oauth_token", None):
            self.get_authenticated_user(self.async_callback(self._on_auth))
            return
        self.authenticate_redirect()

    def _on_auth(self, user):
        if not user:
            raise tornado.web.HTTPError(500, "Twitter auth failed")
        _user = User(user['id_str'], key='twitter', db=self.db)

        if _user.exists:
            self.set_secure_cookie('user', unicode(int(_user.user_id)))
            self.redir()
        else:
            data = {}
            data['user'] = None
            data['login'] = user
            data['source'] = 'twitter'
            self.set_secure_cookie('login', pickle.dumps(user))
            self.render("welcome.html", **data)


class LocalHandler(BaseHandler):
    @tornado.web.asynchronous
    def get(self, errors=None):
        data = {}
        data['user'] = self.get_current_user()
        data['errors'] = errors
        if data['user']:
            self.redir()
        else:
            self.render('login.html', **data)

    @tornado.web.asynchronous
    def post(self):
        flag = 0
        if self.request.remote_ip in self.data['timeout']:
            if 'time' in self.data['timeout'][self.request.remote_ip]:
                flag = 1
                import datetime
                now = datetime.datetime.now()
                two = datetime.timedelta(minutes=2)
                time = self.data['timeout'][self.request.remote_ip]['time']
                print "Now:", now, "Time:", time
                if time + two > now:
                    delay = time + two - now
                    delay = delay.seconds
                    error = "Too many failed login attempts, please wait %s seconds and try \
                         again." % str(delay)
                    self.get(errors=[error])
                else:
                    self.data['timeout'][self.request.remote_ip]['count'] = 0

        if not flag:
            try:
                _username = self.request.arguments['username'][0]
                _password = self.request.arguments['password'][0]
            except:
                _username = None
                _password = None

            data = {}
            data['user'] = self.get_current_user()
            data['username'] = _username
            data['password'] = _password
            data['results'] = None
            data['errors'] = []

            if data['user']:
                #logged in already, quit trying to twice
                self.redir()
            else:
                if not data['username'] or not data['password']:
                    data['errors'].append('Missing field.')

                if not data['errors']:
                    user = User(data['username'], key='username', db=self.db)
                    if not user.exists:
                        try:
                            self.data['timeout'][self.request.remote_ip]['count'] += 1
                            if self.data['timeout'][self.request.remote_ip]['count'] >= 6:
                                import datetime
                                now = datetime.datetime.now()
                                self.data['timeout'][self.request.remote_ip]['time'] = now
                        except:
                            self.data['timeout'][self.request.remote_ip] = {}
                            self.data['timeout'][self.request.remote_ip]['count'] = 0
                        data['errors'].append("Username doesn't exist.")
                    elif user.check_password(data['password']):
                        self.set_secure_cookie('user', unicode(int(user.user_id)))
                    else:
                        #bad password
                        try:
                            self.data['timeout'][self.request.remote_ip]['count'] += 1
                            if self.data['timeout'][self.request.remote_ip]['count'] >= 6:
                                print 'count >= 6, setting time'
                                import datetime
                                now = datetime.datetime.now()
                                self.data['timeout'][self.request.remote_ip]['time'] = now
                        except:
                            self.data['timeout'][self.request.remote_ip] = {}
                            self.data['timeout'][self.request.remote_ip]['count'] = 0
                        count = self.data['timeout'][self.request.remote_ip]['count']
                        data['errors'].append('Bad Password.  %s failed logins.' % str(count + 1))
                else:
                    data['errors'].append('Invalid username.')

                if not data['errors']:
                    self.redir()
                else:
                    self.get(data['errors'])


class LogoutHandler(BaseHandler):
    @tornado.web.authenticated
    @tornado.web.asynchronous
    def get(self):
        if self.get_current_user():
            self.logout_user()
        self.redir()
