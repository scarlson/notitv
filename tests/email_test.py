#!/usr/bin/env python
import contextlib

from tornado import ioloop

from tornadomail import send_mail
from tornadomail.backends.smtp import EmailBackend

from tornado import stack_context


def callback(*args, **kwargs):
    ioloop.IOLoop.instance().stop()
    print 'Successfully sent'

def error_handler(e, msg, traceback):
    try:
        yield
    except:
        pass
    print 'Error:',
    print msg
    ioloop.IOLoop.instance().stop()

with stack_context.ExceptionStackContext(error_handler):
    send_mail(
        'subject', 'message', 'steve@scarlson.co',
        ['steve@scarlson.co'], callback=callback,
        connection=EmailBackend(
            'smtp.gmail.com', 587, 'steve@scarlson.co', 'ascii098',
            True
         )
    )

ioloop.IOLoop.instance().start()