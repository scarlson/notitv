from fabric.api import *
#from paramiko import RSAKey 
 
env.fab_user = "jhacker" 
#env.fab_pkey = RSAKey.from_private_key_file("/path/to/keyfile")

env.git = 'git@bitbucket.org:scarlson/tvdb.git'
env.branch = 'master'
env.git_user = 'scarlson'
env.git_pass = 'ascii098'
env.user = 'steve'
env.password = 'ascii098'
env.commit_message = 'auto_deploy'

def prod():
    """Set the target to production."""
    env.hosts            = ['67.210.247.155']
    env.fab_key_filename = '../'
    env.remote_app_dir   = '/var/web/tvdb'

def deploy():
    """Deploy the site."""
    require('hosts', provided_by = [prod,])
    require('remote_app_dir', provided_by=[prod,])
    local("git add .")
    local("git commit -m '%(commit_message)s'" % env)
    local("git push %(git)s %(branch)s" % env)
    run("cd %(remote_app_dir)s; git pull %(git)s %(branch)s;" % env)
    #run("cd $(remote_app_dir); python tvdb.py")
