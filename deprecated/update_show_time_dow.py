#!/usr/bin/env python
import psycopg2
import urllib2

from xml.etree import ElementTree

the_tv_db_api_key = '17F2768FC31BE4AE'
# http://www.thetvdb.com/api/17F2768FC31BE4AE/episodes/370780/en.xml
# http://www.thetvdb.com/api/17F2768FC31BE4AE/series/252480/all/en.xml

conn = psycopg2.connect('dbname=noti user=postgres')
conn.set_client_encoding('UTF8')
cur = conn.cursor()


def read(url):
    try:
        return urllib2.urlopen(url).read()
    except urllib2.URLError:
        return None
    except urllib2.HTTPError:
        return None


def qry(query):
    try:
        cur.execute(query)
        conn.commit()
    except:
        conn.rollback()


def derp():
    q = cur.mogrify('select show_id, thetvdb_id from id_map;')
    cur.execute(q)
    shows = cur.fetchall()
    for show in shows:
        dow = None
        time = None
        show_id, tvdb_id = show
        url = "http://www.thetvdb.com/api/17F2768FC31BE4AE/series/" + str(tvdb_id) + "/en.xml"
        results = read(url)
        if results:
            try:
                xml = ElementTree.fromstring(results)
                xml = ElementTree.ElementTree(xml).getroot().find('Series')
            except:
                xml = None
            if xml:
                try:
                    dow = xml.find('Airs_DayOfWeek').text
                    time = xml.find('Airs_Time').text
                    if dow or time:
                        q = cur.mogrify('update show set dow = %s, time = %s where show_id = %s;', (dow, time, show_id))
                        qry(q)
                except:
                    pass


if __name__ == "__main__":
    derp()
