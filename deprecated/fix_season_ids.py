#!/usr/bin/env python
import psycopg2
import urllib2
import datetime
from xml.etree import ElementTree

the_tv_db_api_key = '17F2768FC31BE4AE'
# http://www.thetvdb.com/api/17F2768FC31BE4AE/episodes/370780/en.xml
# http://www.thetvdb.com/api/17F2768FC31BE4AE/series/252480/all/en.xml

conn = psycopg2.connect('dbname=noti user=postgres')
conn.set_client_encoding('UTF8')
cur = conn.cursor()

q = cur.mogrify('select max(time) from tvdb_updates;')
cur.execute(q)
last_time = int(cur.fetchone()[0])


def read(url):
    try:
        return urllib2.urlopen(url).read()		
    except urllib2.URLError:
        print "Read URL Error"
        return None
    except urllib2.HTTPError:
        print "Read HTTP Error"
        return None


def qry(query):
    try:
        cur.execute(query)
        conn.commit()
    except:
        conn.rollback()


def update_map_id(series_xml):
    show = series_xml.find('Series')
    if show.find('SeriesName').text:
        s = {}
        s['id'] = show.find('id').text
        s['imdb_id'] = show.find('IMDB_ID').text
        s['zap2it_id'] = show.find('zap2it_id').text
        s['name'] = show.find('SeriesName').text
        query = cur.mogrify('select show_id from show where name like %s;', (s['name'],))
        cur.execute(query)
        show = cur.fetchone()
        query = cur.mogrify('select thetvdb_id, imdb_id, zap2it_id from id_map where show_id = %s;', (show,))
        cur.execute(query)
        res = cur.fetchone()
        if not res:
            try:
                query = cur.mogrify("insert into id_map (show_id, thetvdb_id, imdb_id, zap2it_id) values (%s,%s,%s,%s);", (show, s['id'], s['imdb_id'], s['zap2it_id']))
                qry(query)
            except:
                pass
        if res:
            if (res[0] != s['id']) or (res[1] != s['imdb_id']) or (res[2] != s['zap2it_id']):
                try:
                    query = cur.mogrify('update id_map set thetvdb_id = %s, imdb_id = %s, zap2it_id = %s where show_id = %s;', (s['id'], s['imdb_id'], s['zap2it_id'], show))
                    qry(query)
                except:
                    pass


def update_show(series_xml):
    show = series_xml.find('Series')
    q = cur.mogrify('select show_id from id_map where thetvdb_id = %s;', (show.find('id').text,))
    cur.execute(q)
    try:
        show_id = cur.fetchone()[0]
    except:
        show_id = None
    name = show.find('SeriesName').text
    start_date = show.find('FirstAired').text
    if start_date == '0000-00-00':
        start_date = None
    description = show.find('Overview').text
    network = show.find('Network').text
    rank = show.find('Rating').text
    status = show.find('Status').text
    people = show.find('Actors').text
    genre = show.find('Genre').text
    rating = show.find('ContentRating').text
    dow = show.find('Airs_DayOfWeek').text
    time = show.find('Airs_Time').text
    if show_id:  # show exists, update
        q = cur.mogrify('select * from show where show_id = %s;', (show_id,))
        cur.execute(q)
        q = cur.fetchone()
        if q[1] != name or q[2] != start_date or q[3] != description or q[4] != network or q[5] != rank or q[6] != status or q[7] != people or q[8] != genre or q[9] != rating:		
            try:
                query = cur.mogrify('update show set name = %s, start_date = %s, description = %s, network = %s, rank = %s, status = %s, people = %s, genre = %s, rating = %s, dow = %s, time = %s where show_id = %s;', (name, start_date, description, network, rank, status, people, genre, rating, dow, time, show_id))
                qry(query)
            except:
                pass
    else:  # show doesn't exist, insert
        try:
            query = cur.mogrify('insert into show (name, start_date, description, network, rank, status, people, genre, rating, dow, time) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);', (name, start_date, description, network, rank, status, people, genre, rating, dow, time))
            qry(query)
        except:
            pass


def update_seasons(series_xml):
    seas = []
    if series_xml.find('Series'):
        seriesid = series_xml.find('Series').find('id').text
    else:
        seriesid = series_xml.find('Episode').find('seriesid').text
    for ep in series_xml[1:]:
        s = {}
        if ep.find('SeasonNumber').text not in seas:
            seas.append(ep.find('SeasonNumber').text)
    q = cur.mogrify('select show_id from id_map where thetvdb_id = %s;', (seriesid,))
    cur.execute(q)
    try:
        show_id = cur.fetchone()[0]
    except:
        show_id = None

    for sea in seas:
        if show_id:
            q = cur.mogrify('select season_id from season where show_id = %s and season_no = %s;', (show_id, sea))
            cur.execute(q)
            try:
                s = cur.fetchone()[0]
            except:
                s = None
            if not s:
                try:
                    sea = str(int(float(sea)))
                except:
                    pass
                try:
                    query = cur.mogrify('insert into season (show_id, season_no) values (%s, %s);', (show_id, sea))
                    qry(query)
                except:
                    pass


def update_episodes(series_xml):
    seas = []
    if series_xml.find('Series'):
        seriesid = series_xml.find('Series').find('id').text
    else:
        seriesid = series_xml.find('Episode').find('seriesid').text
    q = cur.mogrify('select show_id from id_map where thetvdb_id = %s;', (seriesid,))
    cur.execute(q)
    try:
        show_id = cur.fetchone()[0]
        print "show_id =", show_id
    except:
        show_id = None
    if show_id:
        for ep in series_xml:
            if ep.tag == "Episode":
                s = {}
                seas = str(int(float(ep.find('SeasonNumber').text)))
                try:
                    query = cur.mogrify("select season_id from season where show_id = %s and season_no = %s;", (show_id, seas))
                except:
                    query = None
                if query:
                    cur.execute(query)
                    season_id = cur.fetchone()[0]
                    s['name'] = ep.find('EpisodeName').text
                    s['desc'] = ep.find('Overview').text
                    s['air_date'] = ep.find('FirstAired').text
                    if s['air_date'] == '0000-00-00':
                        s['air_date'] = None
                    s['se_no'] = str(int(float(ep.find('SeasonNumber').text)))
                    s['ep_no'] = str(int(float(ep.find('EpisodeNumber').text)))
                    s['imdb_id'] = ep.find('IMDB_ID').text
                    if s['name']:
                        q = cur.mogrify('select episode_id from episode where episode_no = %s and season_no = %s and show_id = %s;', (s['ep_no'], s['se_no'], show_id))
                        cur.execute(q)
                        try:
                            q = cur.fetchone()[0]
                        except:
                            q = None
                        if q:
                            query = cur.mogrify("update episode set season_id = %s, name = %s, description = %s, air_date = %s, imdb_id = %s where episode_id = %s;", (season_id, s['name'], s['desc'], s['air_date'], s['imdb_id'], q))
                            qry(query)
                        else:
                            query = cur.mogrify("insert into episode (show_id, season_id, name, description, air_date, episode_no, season_no, imdb_id) values (%s,%s,%s,%s,%s,%s,%s,%s);", (show_id, season_id, s['name'], s['desc'], s['air_date'], s['ep_no'], s['se_no'], s['imdb_id']))
                            qry(query)

def update_episode(ep_id):
    url = 'http://www.thetvdb.com/api/' + the_tv_db_api_key + '/episodes/' + str(ep_id) + '/en.xml'
    ep = read(url)
    if ep:
        try:
            xml = ElementTree.fromstring(ep)
            xml = ElementTree.ElementTree(xml).getroot()
        except:
            xml = None
        if xml:
            update_seasons(xml)
            update_episodes(xml)


def full_show(show_id):
    url = 'http://www.thetvdb.com/api/' + the_tv_db_api_key + '/series/' + str(show_id) + '/all/en.xml'
    show = read(url)
    if show:
        try:
            xml = ElementTree.fromstring(show)
            xml = ElementTree.ElementTree(xml).getroot()
        except:
            xml = None
        if xml:
            if (xml.find('Series').find('SeriesName').text is not None):
                show = xml.find('Series')
                tvdb_id = show.find('id').text
                q = cur.mogrify('select show_id from id_map where thetvdb_id = %s;', (tvdb_id,))
                cur.execute(q)
                try:
                    show_id = cur.fetchone()[0]
                except:
                    show_id = None
                
                status = None
                if show_id:
                    q = cur.mogrify('select status from show where show_id = %s;', (show_id,))
                    cur.execute(q)
                    status = cur.fetchone()[0]

                flag = 0
                if status:
                    if (status == 'Continuing' and xml.find('Series').find('Status').text in ['Ended', None]):
                        flag = 1

                if xml.find('Series').find('Status').text == 'Continuing' or flag:
                    update_show(xml)
                    update_map_id(xml)
                    update_seasons(xml)
                    update_episodes(xml)
                    
def fix_season_id():    
    query = cur.mogrify('select distinct show_id from episode where season_id is NULL;')
    cur.execute(query)
    bad_season = cur.fetchall()
    for show in bad_season:
        q = cur.mogrify('select distinct thetvdb_id from id_map where show_id = %s;', (show[0],))
        cur.execute(q)
        q = cur.fetchone()
        if q:
            full_show(q[0])
        
        
if __name__ == "__main__":
    fix_season_id()
