import os
import sys
import paramiko

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

env = {}
#env['pkey'] = paramiko.RSAKey.from_private_key_file("./id_rsa.pub")
env['git'] = 'git@bitbucket.org:scarlson/tvdb.git'
env['branch'] = 'master'
env['srv_user'] = 'steve'
env['srv_pass'] = 'ascii098'
env['hosts'] = ['67.210.247.155']
env['deploy_dir'] = '/var/web/tvdb'


def local(cmd):
    os.system(cmd)


def run(cmd):
    for host in env['hosts']:
        ssh.connect(host, username=env['srv_user'], password=env['srv_pass'])
        stdin, stdout, stderr = ssh.exec_command(cmd)
        if stderr:
            print [x for x.readline() in stderr]
        if stdout:
            print [x for x.readline() in stdout]
        if stdin:
            print [x for x.readline() in stdin]


def deploy(message='auto-deploy'):
    local('git add .')
    local('git commit -m %s' % message)
    run('cd %(deploy_dir)s;git pull;' % env)

if __name__ == "__main__":
    deploy(sys.argv[-1])
