from models.Episode import Episode
from models.Base import Base


class Season(Base):
    def __init__(self, show_id=None, season_id=None, description=None,
                 start_date=None, end_date=None, season_no=None, image=None):
        self.show_id = show_id
        self.season_id = season_id
        self.description = description
        self.start_date = start_date
        self.end_date = end_date
        self.season_no = season_no
        self.image = image

    def __eq__(self, notself):
        if isinstance(notself, Season):
            return self.season_id == notself.season_id
        return 0

    @property
    def episodes(self):
        if not hasattr(self, '_episodes'):
            query = '''select * from episode where season_id = %s
            order by season_no::int asc, episode_no::int asc;'''
            params = self.season_id
            results = self.select(query, params)
            self._episodes = []
            if results:
                if isinstance(results[0], tuple):
                    for result in results:
                        self._episodes.append(Episode(*result))
                elif isinstance(results[0], int):
                    self._episodes.append(Episode(*results))
        return self._episodes

    def get_episode_by_seq(self, ep_no):
        query = """select * from episode where episode_no::int = %s
        and season_id = %s;"""
        params = (ep_no, self.season_id)
        results = self.select(query, *params)
        return Episode(*results)
