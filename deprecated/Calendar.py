from models.Show import Show
from models.Episode import Episode
from models.Base import Base

import datetime
today = datetime.date.today()
week = datetime.timedelta(days=6)
next_week = today + week


class Calendar(Base):
    def __init__(self, subs=None):
        self.subs = subs
    
    @property
    def cals(self):
        if not hasattr(self, '_cals'):
            self._cals = []
            eps = []
            if self.subs:
                query = """select * from episode where
                    air_date::date >= %s and air_date::date <= %s;"""
                params = (today, next_week)
                results = self.select(query, *params)
                if results:
                    weeklys = []
                    subbin = set([show.show_id for show in self.subs])
                    
                    if isinstance(results[0],tuple):
                        for res in results:
                            if res[0] in subbin:
                                eps.append(res)
                    elif isinstance(results[0],int):
                        eps.append(results)
            else:
                return None
            shows = list((Show(episode[0]), Episode(*episode)) for episode in eps if episode[5] is not None)
            week = {}
            derp = []
            for i in range(7):
                week[i] = []
                for show, episode in shows:
                    if episode.air_date.date() == (today + datetime.timedelta(days=i)):
                        week[i].append((show,episode))
            for x in range(len(max([week[key] for key in week],key=len))):
                derps = []
                for i in range(7):
                    try:
                        derps.append(week[i].pop())
                    except IndexError:
                        pass
                derp.append(derps)
                        
            self._cals = derp
        return self._cals
        
