from whoosh.fields import *

schema = Schema(
        title=TEXT(stored=True), 
        url=ID(stored=True, unique=True), 
        desc=ID(stored=True), 
        rank=NUMERIC(stored=True, type=float),         
        name = TEXT(field_boost=5.0)
        start_date = DATETIME
        description = TEXT(stored=True),
        network = TEXT,
        rank = NUMERIC,
        rating = TEXT, # TV-MA, etc
        dow = TEXT,
        time = TEXT,
        image = BOOLEAN,
        hidden = BOOLEAN
        )