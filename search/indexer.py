import os

from whoosh import index
from whoosh.fields import *

schema = Schema(
        show_id = ID(stored=True),
        name = TEXT(stored=True,field_boost=5.0),
        description = TEXT,
        rank = NUMERIC(type=float)
        )

_ix = None

def get_index():
    global _ix

    if _ix is not None:
        pass
    elif not os.path.exists("/var/web/tvdb/index"):
        os.mkdir("/var/web/tvdb/index")
        _ix = index.create_in("/var/web/tvdb/index", schema)
    else:
        _ix = index.open_dir("/var/web/tvdb/index")

    return _ix

def get_writer():
    return get_index().writer()

def get_searcher():
    return get_index().searcher()
