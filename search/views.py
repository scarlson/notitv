from whoosh.qparser import QueryParser

from search.indexer import get_searcher, schema
from models.Show import Show


def search(req):
    searcher = get_searcher()
    q = QueryParser("name", schema=schema).parse(unicode(req))
    results = searcher.search(q, limit=100)

    return results
