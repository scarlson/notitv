from handlers.StaticPages import IndexHandler
from handlers.StaticPages import AboutHandler
from handlers.StaticPages import FaqHandler

from handlers.ShowData import SearchHandler
from handlers.ShowData import ShowHandler
from handlers.ShowData import AllCalendarHandler

from handlers.Authenticators import GoogleHandler
from handlers.Authenticators import FacebookHandler
from handlers.Authenticators import TwitterHandler
from handlers.Authenticators import LocalHandler
from handlers.Authenticators import WelcomeHandler
from handlers.Authenticators import LogoutHandler

from handlers.UserHandler import AccountHandler
from handlers.UserHandler import NotificationHandler
from handlers.UserHandler import CalendarHandler
from handlers.UserHandler import SubsHandler
from handlers.UserHandler import EmailValidateHandler
from handlers.UserHandler import EmailSubHandler
from handlers.UserHandler import EmailUnsubHandler

from handlers.Subscriptions import SubHandler
from handlers.Subscriptions import UnsubHandler

from handlers.AccountManagement import PassChangeHandler
from handlers.AccountManagement import ResetHandler
from handlers.AccountManagement import DeleteHandler
from handlers.AccountManagement import RegisterHandler

from handlers.Moderation import ToggleHandler
from handlers.Moderation import ListHandler
from handlers.Moderation import ShowEditHandler
from handlers.Moderation import ShowDeleteHandler
from handlers.Moderation import ShowImageHandler
from handlers.Moderation import ShowImage2Handler
from handlers.Moderation import ShowRefreshHandler
from handlers.Moderation import ShowAddHandler
from handlers.Moderation import HiddenHandler
from handlers.Moderation import ImagesHandler
from handlers.Moderation import Images2Handler
from handlers.Moderation import DowHandler
from handlers.Moderation import TimeHandler
from handlers.Moderation import DescHandler
from handlers.Moderation import NetworksHandler
from handlers.Moderation import NetworkHandler
from handlers.Moderation import NetworkDeleteHandler
from handlers.Moderation import UserHandler
from handlers.Moderation import UserDeleteHandler
from handlers.Moderation import UsersHandler
from handlers.Moderation import ErrorsHandler
from handlers.Moderation import ErrorViewHandler

from handlers.TestHandler import TestHandler

from handlers.ErrorHandler import FourOhFourHandler

import psycopg2
from data import data
#data = {'users': {}, 'shows': {}, 'timeout': {}}
data['db'] = psycopg2.connect('dbname=noti user=postgres')
data['db'].set_client_encoding('UTF8')


url_patterns = [
    (r"/", IndexHandler, dict(data=data)),
    (r"/test", TestHandler, dict(data=data)),
    (r"/faq", FaqHandler, dict(data=data)),
    (r"/about", AboutHandler, dict(data=data)),
    (r"/mod/list", ListHandler, dict(data=data)),
    (r"/mod/users", UsersHandler, dict(data=data)),
    (r"/mod/user/([0-9]+)", UserHandler, dict(data=data)),
    (r"/mod/user/delete/([0-9]+)", UserDeleteHandler, dict(data=data)),
    (r"/mod/hidden", HiddenHandler, dict(data=data)),
    (r"/mod/images", ImagesHandler, dict(data=data)),
    (r"/mod/images2", Images2Handler, dict(data=data)),
    (r"/mod/dow", DowHandler, dict(data=data)),
    (r"/mod/time", TimeHandler, dict(data=data)),
    (r"/mod/desc", DescHandler, dict(data=data)),
    (r"/mod/networks", NetworksHandler, dict(data=data)),
    (r"/mod/network/delete/(.*)", NetworkDeleteHandler, dict(data=data)),
    (r"/mod/network/(.*)", NetworkHandler, dict(data=data)),
    (r"/mod/show/edit/([0-9]+)", ShowEditHandler, dict(data=data)),
    (r"/mod/show/image/([0-9]+)", ShowImageHandler, dict(data=data)),
    (r"/mod/show/image2/([0-9]+)", ShowImage2Handler, dict(data=data)),
    (r"/mod/show/delete/([0-9]+)", ShowDeleteHandler, dict(data=data)),
    (r"/mod/show/toggle/([0-9]+)", ToggleHandler, dict(data=data)),
    (r"/mod/show/refresh/([0-9]+)", ShowRefreshHandler, dict(data=data)),
    (r"/mod/show/add/([0-9]+)", ShowAddHandler, dict(data=data)),
    (r"/mod/error/([0-9]+)", ErrorViewHandler, dict(data=data)),
    (r"/mod/errors", ErrorsHandler, dict(data=data)),
    (r"/u/email/validate", EmailValidateHandler, dict(data=data)),
    (r"/u/email/([a-zA-Z0-9]+)", EmailSubHandler, dict(data=data)),
    (r"/u/unsub/([a-zA-Z0-9]+)", EmailUnsubHandler, dict(data=data)),
    (r"/u/reset", ResetHandler, dict(data=data)),
    (r"/u/password", PassChangeHandler, dict(data=data)),
    (r"/u/delete", DeleteHandler, dict(data=data)),
    (r"/about", AboutHandler, dict(data=data)),
    (r"/register", RegisterHandler, dict(data=data)),
    (r"/subscribe", SubHandler, dict(data=data)),
    (r"/unsubscribe", UnsubHandler, dict(data=data)),
    (r"/account", AccountHandler, dict(data=data)),
    (r"/noti", NotificationHandler, dict(data=data)),
    (r"/all", AllCalendarHandler, dict(data=data)),
    (r"/cal", CalendarHandler, dict(data=data)),
    (r"/subs", SubsHandler, dict(data=data)),
    (r"/logout", LogoutHandler, dict(data=data)),
    (r"/login", LocalHandler, dict(data=data)),
    (r"/login/google", GoogleHandler, dict(data=data)),
    (r"/login/facebook", FacebookHandler, dict(data=data)),
    (r"/login/twitter", TwitterHandler, dict(data=data)),
    (r"/search", SearchHandler, dict(data=data)),
    (r"/welcome", WelcomeHandler, dict(data=data)),
    (r"/s/([0-9]+)", ShowHandler, dict(data=data)),
    (r".*", FourOhFourHandler, dict(data=data))
]
